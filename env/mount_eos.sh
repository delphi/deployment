#!/bin/bash
if [ -d /eos/opendata/delphi ]; then
    echo "EOS is available"
    exit 0
fi
if [ -e /usr/bin/eosxd ]; then
    grep container= /proc/1/environ > /dev/null 2>&1
    res=$?
    if [ $res -eq 0 ]; then
	echo "Trying to mount EOS..."
	sudo mkdir -p /eos/opendata/delphi
	sudo /usr/bin/eosxd -ofsname=eospublic.cern.ch:/eos/opendata/delphi /eos/opendata/delphi
    fi
fi
