#!/bin/bash


# create working directory
topdir=`pwd`
swcddir=$1
mkdir $swcddir
cd $swcddir

tag=`date +%Y-%m-%d`
# get code
for project in deployment quickstart examples docs mc-production idea cernlib event-server dstana delgra patchy database ; do
    git clone https://:@gitlab.cern.ch:8443/delphi/${project}.git
    tar -zcf ${project}-${tag}.tgz $project
    rm -rf $project
done

# get simana versions
for version in va0u va0e v99e v98e v97g v96g v95d v94c v93d v92e; do
    git clone https://:@gitlab.cern.ch:8443/delphi/simana_${version}.git
    tar -zcf simana_${version}-${tag}.tgz simana_${version}
    rm -rf simana_${version}
done
