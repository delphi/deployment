#!/bin/bash
rct=0
dir=`pwd`

for test in `find tests -name \*.sh`; do
    echo "Executing test $test"
    cd $dir
    . $DELPHI_INSTALL_DIR/setup.sh
    ${test}
    rc=$?
    if [ "x$rc" = "x0" ]; then
	echo "Test succeeded"
    else
	echo "Test FAILED"
	rct=1
    fi
done
exit $rct
