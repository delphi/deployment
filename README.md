# Compile and deploy the DELPHI software stack
This repository contains everything which is needed to compile the full DELPHI software stack. Binaries are created by a gitlab CI and can be used directly.

## Deployment scripts
The scripts in this repository will

* fetch the code from gitlab
* compile everything on supported platforms and store the binaries in gitlab.
* run basic tests

Essentially, the can be used to create a new release on CVMFS or a new software CD for archival

## Notes
* Most recent Linux version are supported
* The default is to compile in 64bit mode. Legacy 32bit compiles are possible by setting
```
export LEGACY=' -m32 '
```

### Prerequisites
DELPHI software relies on CERNLIB. All builds use the community CERNLIB from gitlab.cern.ch/dphep/cernlib/cernlib which is maintained on a best effort basis and provides 64bit builds thanks to a hack done by a Debian developers a couple of years ago. We are greatful to the Debian team for these contributions!

To build and run the stack, the follwoing packages are required:

* gcc and gfortran with 32bit support, including static libraries
* X11 development libraries
* Motif development libraries
* OpenGL and development libraries
* Xorg fonts
* tcsh

### XRootD support
For Linux versions released in about 2022 and later, the stack can be build with XRootD support in CERNLIB. To enable XRootD at compile time, make sure that xrootd-client and development files are installed, and set

```console
export CERNLIB_XROOTD=ON
```
in bash or 
```console
setenv CERNLIB_XROOTD ON
```
for (t)csh.
The CI will create enable this for

* Ubuntu 22 and newer
* Debian 12 and newer
* Alma9/RHEL9 and newer (64bit only)


### Supported modules
The following DELPHI software components are supported by this script

* cernlib (from https://gitlab.cern.ch/dphep)
* dstana
* simana for the years 1992-2000
* delgra
* DELPHI event server
* OpenPhigs with DELPHI extensions for Fortran bindings
* Idea (unmaintained)

### Unsupported modules
* WIRED (32bit, 64bit)

### Currently support systems
* CC7 64bit (32bit, 64bit)
* AlmaLinux 8 (32bit, 64bit)
* AlmaLinux 9 (32bit, 64bit)
* RHEL8 (32bit, 64bit)
* RHEL9 (32bit, 64bit)
* Ubuntu 20 (64bit)
* Ubuntu 22 (64bit)
* Ubuntu 24 (64bit)
* Debian 12 (64bit)

Binaries are available on /cvmfs/delphi.cern.ch. Some older binaries have been archived in /cvmfs/delphi.cern.ch/attic.
They are discontinued and not up to date.

## Platform specific notes

### XRootD support

The xrootd protocol is supported on the following architectures, and only for CERNLIB builds with cmake:

* Alma 9 x86_64 and ARM64 (and compatible)
* Ubuntu 22 and newer
* Debian 12 and newer

and compatible platforms. With these versions it is possible to read data directly over the network, and there is no need to copy over the samples or mount the EOS file system locally.

### Notes on CentOS7

CentOS7 ships with gfortran 4.8.5. CERNLIB needs to be compiled with an additional flag to switch off aggressive loop optimisations which otherwise break bits of hbook. 

Make sure that you have the following packages installed:

For 32bit:
Please note that 32bit CC7 builds are currently no longer supported. In case they are needed e.g. for validation, the may be re-added later on.
The following packages are required:

```bash
# yum install cmake3 freetype-devel.i686 glibc-devel.i686 glibc-headers glibc-headers.i386 glibc-headers.i686 libgfortran5.i686 libgfortran.i686 libpng-devel.i686 libSM-devel.i686 libstdc++-devel.i686 libX11-devel.i686 libXfont2-devel.i686 libXfont-devel.i686 libXft-devel.i686 libXft-devel.i686 libXmu-devel.i686 libXpm-devel.i686 libXt-devel.i686 motif-devel.i686 tcsh xorg-x11-fonts-100dpi xorg-x11-fonts-75dpi.noarch mesa-libGL-devel.i686 mesa-libGLU-devel.i686 libSM-devel.i686 libXaw-devel.i686 libICE-devel.i686
```

For 64bit you need:
```
#  yum -y install cmake3 freetype-devel glibc-devel glibc-headers glibc-headers glibc-headers git imake libpng-devel libSM-devel libstdc++-devel libX11-devel libXfont2-devel libXfont-devel libXft-devel libXft-devel libXmu-devel libXpm-devel libXt-devel motif-devel tcsh xorg-x11-fonts-100dpi xorg-x11-fonts-75dpi.noarch gcc gcc-c++ gcc-gfortran mesa-libGL-devel mesa-libGLU-devel libSM-devel libXaw-devel libICE-devel
```

Note that the build requires a more recent version of cmake than the OS provides. There is cmake3 which works. You may need to remove the cmake package and set a link from cmake3 to cmake to ensure that cmake3 is used.

### Notes on Alma 8 and 9

Alma8 and Alma 9 are binary compatible with RHEL8 and RHEL9. CentOS Stream is no longer suppported and therefore no new builds will be provided for these.
The following packages are needed:

```bash
# dnf install freetype-devel gcc gcc-c++ gcc-gfortran git glibc-devel glibc-headers imake libgfortran libnsl2-devel libnsl2 libSM-devel libSM libstdc++-devel libX11-devel libXaw-devel libXfont2-devel libXft-devel libXmu-devel libXpm-devel mesa-libGL-devel mesa-libGLU-devel mesa-libGLw-devel motif-devel tcsh xorg-x11-fonts-100dpi xorg-x11-fonts-75dpi libGLEW glew-devel
```
32bit builds require the corresponding 32bit packages. Please note that DELGRA is not supported on Alma9 32bit.

### Notes on Fedora

Fedora is similar to Alma. Please refer to the package lists for these. Fedora builds have been done in the past and worked. 

### Notes on Ubuntu and Debian
As of Ubuntu 20 there are no 32bit Motif development libraries available any longer, therefore only the 64bit stack can be compiled and shipped.

Please ensure you have at least these libraries installed:
```bash
apt install cmake gcc g++ gfortran libx11-dev libx11-dev libx11-6 libx11-6 libglu1-mesa libglu1-mesa-dev xutils-dev libmotif-dev libmotif-common r-base-dev tcsh xfonts-100dpi xfonts-75dpi libxfont2  xutils-dev libxbae-dev libxaw7-dev libssl-dev libglew-dev libdlm-dev
```
The CMake version shipped with Ubuntu 18.04 is too old to be used with the community CERNLIB. As a work around, get a recent version in source code first, compile and install into /usr/local.

On Ubuntu 18, for 32bit, please ensure that you have at least these packages installed:
```bash
# apt install cmake gcc--multilib g++-4.8-multilib gfortran-4.8-multilib libx11-dev libx11-dev:i386 libx11-6 libx11-6:i386 libglu1-mesa:i386 libglu1-mesa-dev:i386 xutils-dev libmotif-dev:i386 libmotif-common:i386 r-base-dev tcsh xfonts-100dpi xfonts-75dpi libxfont2:i386
```
### Notes on Apple

For this platform, binaries are not provided, and they need to be compiled locally. There are known issues on this platform, specifically

* DELGRA does not show the event headers
* 92e dstana results in a Zebra error and goes into a loop

The build only works if the legacy linker is used. XRootD support works, but requires XRootD version 5.8.0 or newer.

## Notes on DELGRA

DELGRA is based on OpenPHIGS with self-implemented Fortran bindings. OpenPHIGS is free software licensed under LGPL2, so no license is required any longer to run the event display. This move was needed for 64bit builds, and works actually best when run locally when hardware acceleration via OpenGL is available.

Please check https://gitlab.cern.ch/delphi/delgra for more information.

## Compiling

Generally, there is no need for end users to recompile the stack. Binaries are made available via /cvmfs/delphi.cern.ch. If additional binaries are needed, please get in touch with the DELPHI core computing group to see what can be done.

A CERN authenticated account is required to read the software repositories on gitlab.cern.ch.
In addition, you need to have access to https://gitlab.cern.ch/dphep/cernlib/cernlib which is the case for any CERN account.

Ensure that you have enough free space, at least 8GB would be ok.
Install the dependencies as mentioned above, for the OS you use.

### Preparation
First, create a working directory and clone this project
```
kinit
git clone https://:@gitlab.cern.ch:8443/delphi/deployment.git
cd deployment
```
Then you can build all components. Set the environment variable ```DELPHI_INSTALL_DIR``` to
determine where the code should go to. If you are working on a machine which has /cvmfs mounted, DELPHI_INSTALL_DIR and CERN may point to /cvmfs.

### Building the full 64bit stack
As of 28/9/2022 64bit builds will use the community cernlib with ```cmake``` instead of ```imake```. This allows for builds on ```aarch64```.
```bash
export DELPHI_INSTALL_DIR=~/delphi64
make all > make_all.log 2>&1
```

### Building the full 32bit stack
```bash
export DELPHI_INSTALL_DIR=~/delphi32
export LEGACY='-m32'
make all > make_all.log 2>&1
```

Individual modules can be (re-)build one by one, respecting dependencies.

## Contributing
Contributions and issue reports is highly appreciated. Feel free to send patches and/or bug reports.
