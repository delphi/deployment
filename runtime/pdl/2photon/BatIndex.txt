# BatCave index file. Automatically generated. Do not remove
# nickname                                       year       proc   category  generator    channel        lab     nfiles    nevents     minrun     maxrun        ecm       mass       xsec     target   settings      mass2
xs_gpym6143c1_e206.7_c99_1l_a1                 2000      v99a1      4FERM   gpym6143         c1       cern       1499    1498998      20000      21504      206.7          -          -          -          -          -
xs_gpym6143c2_e206.7_c99_1l_a1                 2000      v99a1      4FERM   gpym6143         c2       cern        987     987000      21500      22500      206.7          -          -          -          -          -
xs_gpym6147c3_e200_c99_1l_a1                   1999      v99a1      4FERM   gpym6147         c3       cern        200     200000      22422      22700        200          -          -          -          -          -
xs_gpym6156c4_e200_l99_1l_a1                   1999      v99a1      4FERM   gpym6156         c4       lyon        140     139753      20000      20139        200          -          -          -          -          -
xs_gpym6156jpsi_e200_c99_1l_a1                 1999      v99a1      4FERM   gpym6156       jpsi       cern         59      59000       9001       9060        200          -          -          -          -          -
xs_gpym_e200_c99_1l_a1                         1999      v99a1      4FERM   gpym6143          -       cern       9000    8995332      10001      19000        200          -          -          -          -          -
xs_gpym_e200_l99_1l_a1                         1999      v99a1      4FERM   gpym6143          -       lyon       1000    1000000      19001      20000        200          -          -          -          -          -
