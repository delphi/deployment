# BatCave index file. Automatically generated. Do not remove
# nickname                                       year       proc   category  generator    channel        lab     nfiles    nevents     minrun     maxrun        ecm       mass       xsec     target   settings      mass2
xs_zgpy6156b1_e189_c99_1l_a1                   1998      v99a1      2FERM   zgpy6156         b1       cern        300     299935      10001      10300        189          -          -          -          -          -
xs_zgpy6156b1_e200_c99_1l_a1                   1999      v99a1      2FERM   zgpy6156         b1       cern        300     299940      10001      10300        200          -          -          -          -          -
xs_zgpy6156b1_e91.2_c99_1l_a1                  1999      v99a1      2FERM   zgpy6156         b1       cern        100      99993      10001      10100       91.2          -          -          -          -          -
xs_zgpy6156b2_e189_c99_1l_a1                   1998      v99a1      2FERM   zgpy6156         b2       cern        100      99981      10210      10400        189          -          -          -          -          -
xs_zgpy6156b2_e200_c99_1l_a1                   1999      v99a1      2FERM   zgpy6156         b2       cern        100      99350      10231      10400        200          -          -          -          -          -
xs_zgpy_e189_c99_1l_a1                         1998      v99a1      2FERM   zgpy6156          -       cern        300     299935      10001      10300        189          -          -          -          -          -
xs_zgpy_e200_c99_1l_a1                         1999      v99a1      2FERM   zgpy6156          -       cern        300     299940      10001      10300        200          -          -          -          -          -
