package   Delphi::Util;
require   Exporter;
@ISA    = qw(Exporter);
@EXPORT = qw(ExpandSequence CompactifySequence);

#############################################################

# Expand a sequence like "1,3-6" into "1 3 4 5 6"

sub ExpandSequence {
    my $seq = shift;
    return () if not defined $seq;
    return () if not $seq =~ /^[0-9\-,]+$/; # sanity check
    my @output;
    for (split(/,/,$seq)) {
        if ( /-/ ) {
            if (my ($first,$last) = /^(\d+)\-(\d+)$/) {
                #$first = $1 if $first =~ /^0+(\d+)$/; # remove leading zero's
                #$last  = $1 if $last  =~ /^0+(\d+)$/; # remove leading zero's
                map {s/^0+(\d+)/$1/} ($first,$last); # remove leading zero's
                push(@output,$first..$last);
            }
        }else{
            s/^0+(\d+)/$1/; # remove leading zero's
            #$_ = $1 if /^0+(\d+)$/; # remove leading zero's
            push(@output,$_)
        }
    }
    # sort and remove double entries
    @output = sort {$a <=> $b} @output;
    my @seq = shift @output;
    for (@output){
        push(@seq,$_) if $_ != $seq[-1];
    }
    return @seq;
}

#############################################################

# Compactify a sequence like "1 3 4 5 6" into "1,3-6"

sub CompactifySequence{
    my @seq = @_;
    map {s/^0+(\d+)/$1/} @seq; # remove leading zero's
    return "@seq" if scalar(@seq) == 1;
    # @seq = (1 3 4 6 7 8 18)
    my $str = $seq[0]; my $i;
    for ($i=1;$i<=$#seq;$i++){
        $str .= ($seq[$i] == $seq[$i-1]+1?"-":",").$seq[$i];
    } # $str = "1,3-4,6-7-8,18"
    undef @seq;
    for (split(/,/,$str)) {
        if (/^(\d+)-.*.-(\d+)$/){
            $_ = "$1-$2";
        }
        push(@seq,$_);
    } # @seq = (1 3-4 6-8 18)
    return join(",",@seq);
}

#############################################################

1;
