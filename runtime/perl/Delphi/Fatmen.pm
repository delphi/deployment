package   Delphi::Fatmen;
require   Exporter;
@ISA    = qw(Exporter);
@EXPORT = qw(LoadFatmen ExpandContent ExpandRuns ExpandSequence ListOfNames);
#
# dummy version, uses only plain text files
#
use Data::Dumper;

sub LoadFatmen {
    my $nick = shift;
    my %catalog;
    #
    # only a nickname needed?
    #
    if (defined $nick){
        $nick = lc($nick);
	%catalog = LoadPlainTextFile($nick);
    }
    #
    # return the result
    #print Dumper(\%catalog);
    return %catalog;
}

##########################################################################
#
# load plain text files
#
sub LoadPlainTextFile {
    my %catalog;
    my $search = shift;
    my $BatCave = $ENV{DELPHI_BATCAVE} || "/afs/cern.ch/delphi/tasks/mcprod/pdl";
    my $indx = "$BatCave/index.txt";
    #print("Reading $BatCave/index.txt\n");
    undef my $pdl;
    if (-e $indx){
	open(IND,"<$indx") or PrintError("Could not open \"$indx\": $!");;
	while (<IND>){
	    do {$pdl = $_;last;} if /$search\.pdl$/;
	}
	close IND;
    }
    return () if not defined $pdl;
    #
    # read the plain text file
    #
    my %fat;
    undef my @cont;undef my @name;
    undef my @runs;undef my @nevt;
    my $name = $1 if ($pdl =~ /\/([\w\.\_]+)\.pdl$/);
    $pdl =~ s/\/afs\/cern.ch\/delphi\/tasks\/mcprod\/pdl/$BatCave/;
    #print "processing $pdl $name\n";
    open(P,"<$pdl");
    while(<P>){
	if (/Nickname:\s*(\S+)/) {
            $name = lc($1) if (not defined $name);
        }
        do {push(@name,$1);next} if (/ name\s+: (\S+)/||/ Name\s+: (\S+)/);
        do {$fat{$name}{Desc} = $1;next} if (/Description\s+: (.*)\s*$/);
        do {$fat{$name}{Comm} = $1;next} if (/Comments\s+: (.*)\s*$/);
	do {push(@runs,$1)} if (/RUN\s*=\s*([\d\-\,]+)/i);
	do {push(@nevt,$1)} if (/NEVT\s*=\s*(\d+)/i);
        do {push(@cont,"$1.$2");next} if (/VID\s*=\s*(\S+),\s*NUM\s*=\s*\((\S+)\)\s*/i);
        do {push(@cont,"$1.$2");next} if (/VID\s*=\s*(\S+),\s*NUM\s*=\s*(\d+)\s*/i);
	#print $_;
        do {push(@cont,$1);next} if (/FAT\s*=\s*(\S+)\s*$/i);
        do {push(@cont,$1);next} if (/FILE\s*=\s*(\S+)\s*/i);
        do {push(@cont,$1);next} if (/NAME\s*=\s*(\S+)\s*/i);
	#
	# handle compressed contents
	#
	next if /^\*/;
	next if /^\s*$/;
	push @cont, $1 if (/^([\da-zA-Z\_\:\-\,\.\[\]]+)/)
    }
    close P;
    $fat{$name}{Name} = join " ",@name if @name;
    $fat{$name}{Cont} = join "\n", @cont if @cont;
    $fat{$name}{Runs} = join "\n", @runs if @runs;
    $fat{$name}{Nevt} = join "\n", @nevt if @nevt;
    $fat{$name}{Desc} = " " if (not defined $fat{$name}{Desc});
    $fat{$name}{Comm} = " " if (not defined $fat{$name}{Comm});
    #print "FATMEN Runs: \"$fat{$name}{Runs}\"\n";
    return %fat;
}

##########################################################################
#
# inflate the "Cont" content field of a dataset
#

sub ExpandContent($$){
    my ($cont,$name) = @_;
    #
    # check where we area reading the data from
    #
    our $DELPHI_DATA_ROOT="/eos/opendata/delphi";
    $DELPHI_DATA_ROOT=$ENV{"DELPHI_DATA_ROOT"} if (defined $ENV{"DELPHI_DATA_ROOT"} && (-d $ENV{"DELPHI_DATA_ROOT"}));
    #
    # inflate the "Cont" fields
    #
    my @recovered = qw(Y00545 Y00547 Y01414 Y01415 Y01421 Y01422 Y01423 Y01424 Y15103 Y15109 Y15131 Y16377);
    my @c3 =qw(Y10567 Y10568 Y27546 Y27547);
    my @tapecorr = qw(Y22670 Y22669 Y23021 Y10624);
    #print "expand content called with $cont,$name\n";
    my @cont = ();
    for my $data (split(/\s+/,$cont)){
        # "Y00565.[1-13].al" --> "/castor/cern.ch/delphi/tape/Y00565/Y00565.[1-13].al"
        if ($data =~ /^([A-Z0-9]{6})\.[0-9\,\-\[\]]+\.[ans]l$/i){
            my $rw = $1;
            my $cdir = "delphi/tape";
	    # check if we are reading from the open data area
	    if ($DELPHI_DATA_ROOT =~ /opendata/){
		$cdir = "delphi/collision-data";
		$cdir = "delphi/simulated-data/misc" if grep {$rw eq $_} @tapecorr;
	    } else {
		$cdir = "delphi/recover" if grep {$rw eq $_} @recovered;
		$cdir = "delphi/tapecorr" if grep {$rw eq $_} @tapecorr;
	    }
	    $cdir = "c3/tape" if grep {$rw eq $_} @c3;
	    if ($name =~ /(^\/castor\/cern\.ch\/delphi\/rawd\/\S+)\/C[\d\-]+$/){
		$data = "$1/$rw/$data";
	    } else {
		$data = "/castor/cern.ch/${cdir}/$rw/$data";
	    }
        }
	if ($name =~ /(^\/castor\/cern\.ch\/delphi\/MCprod\/\S+)\/C[\d\-]+$/){
            $data = "$1/$data" if not ($data =~ /(FILE|NAME)/i);
        }	
	if ($DELPHI_DATA_ROOT =~ /opendata/){
	    $data =~ s/\/MCprod\//\/simulated-data\//;
	    $data =~ s/\/rawd\//\/raw-data\//;
	}
        if ($data =~ /^[A-Z0-9]{6}\.[\d\-,]+$/i){
            my ($vid,$range) = split(/\./,$data);
            map {push(@cont,"$vid.$_")} ExpandSequence($range);
        }elsif ($data =~ /\[([\d\,\-]+)\]/){
            map {push(@cont,"$`$_$'")} ExpandSequence($1);
        }else{
            push(@cont,$data);
        }
    }
    #
    # Update the path to match our data source
    #
    s/\/castor\/cern\.ch\/delphi/$DELPHI_DATA_ROOT/ for(@cont);
    #
    # unfortunately, some data sets have holes...
    #
    #    "xs_wwex_e183_sa97_1l_e2" => {
    #        Name => '//CERN/DELPHI/P02_SIMD/XSDST/WWEX/E183.0/VS36LA43JAN98F2/SACL/SUMT/C1-30,36-75',
    #        Desc => 'XShortDST e+e- -> W+W- (chan-4)        ;Sim.97 ANA97E Fix 2 ; SACL',
    #        Comm => '70 files, total size =    8.925 Gb',
    #        Cont => 'Y16361.[95-124,130-169].al',
    #
    my %cont = ();
    if (defined $name){
	my ($range) = $name =~ /C([0-9\-\,]+)$/i;
	if ($range){
	    my @range = ExpandSequence($range);
	    while(my $i = shift @range){
		my $cont = shift @cont;
		$cont{$i} = $cont;
	    }
	} else {
	    map {$cont{$_+1}=$cont[$_];} (0..$#cont);
	}
    } else {
	map {$cont{$_+1}=$cont[$_];} (0..$#cont);
    }
    #
    #
    #
    return %cont;
}

sub ExpandRuns($$){
    my ($cont,$name) = @_;
    #
    # inflate the "Runs" fields
    #
    my @runs = ();
    for my $data (split(/\s+/,$cont)){
	if ($data =~ /([\d\,\-]+)/){
            map {push(@runs,$_)} ExpandSequence($1);
        }
    }
    #
    # unfortunately, some data sets have holes...
    my %runs = ();
    if (defined $name){
	my ($range) = $name =~ /C([0-9\-\,]+)$/i;
	if ($range){
	    my @range = ExpandSequence($range);
	    while(my $i = shift @range){
		my $cont = shift @runs;
		$runs{$i} = $cont;
	    }
	} else {
	    map {$runs{$_+1}=$runs[$_];} (0..$#runs);
	}
    } else {
	map {$runs{$_+1}=$runs[$_];} (0..$#runs);
    }
    #
    #
    #
    return %runs;
}

sub ListOfNames {
    #
    # returns array of all known nicknames
    #
    my %catalog = LoadFatmen();
    my @list = (keys %catalog);
    #my $BatCave = $ENV{DELPHI_BATCAVE} || "/afs/cern.ch/user/d/delmc/BatMan/Cern/Madmen";
    my $BatCave = $ENV{DELPHI_BATCAVE} || "/afs/cern.ch/delphi/tasks/mcprod/pdl";
    my $indx = "$BatCave/index.txt";
    #print @list;
    if (-e $indx){
	open(IND,"<$indx") or PrintError("Could not open \"$indx\": $!");
	while (<IND>){push @list, $1  if (/\/([\w\.\_]+)\.pdl$/);}
	close IND;
    }
    return (@list);
}
#############################################################

# Expand a sequence like "1,3-6" into "1 3 4 5 6"

sub ExpandSequence {
    my $seq = shift;
    return () if not defined $seq;
    return () if not $seq =~ /^[0-9\-,]+$/; # sanity check
    #print "Expand seq: $seq\n";
    my @output;
    for (split(/,/,$seq)) {
        if ( /-/ ) {
            if (my ($first,$last) = /^(\d+)\-(\d+)$/) {
                #$first = $1 if $first =~ /^0+(\d+)$/; # remove leading zero's
                #$last  = $1 if $last  =~ /^0+(\d+)$/; # remove leading zero's
                map {s/^0+(\d+)/$1/} ($first,$last); # remove leading zero's
                push(@output,$first..$last);
            }
        }else{
            s/^0+(\d+)/$1/; # remove leading zero's
            #$_ = $1 if /^0+(\d+)$/; # remove leading zero's
            push(@output,$_)
        }
    }
    # sort and remove double entries
    #@output = sort {$a <=> $b} @output;
    my @seq = shift @output;
    for (@output){
        push(@seq,$_) if $_ != $seq[-1];
    }
    #print "returning:\n";
    #foreach (@seq){print "$_ "};
    return @seq;
}

1;
