# general path, defaults to cvmfs

if test "x${DELPHI_INSTALL_DIR}" = "x" ; then
    export DELPHI_INSTALL_DIR="/cvmfs/delphi.cern.ch"
else
    export DELPHI_INSTALL_DIR
fi
# reset environment
. ${DELPHI_INSTALL_DIR}/unset.sh

#
if [ -e $DELPHI_INSTALL_DIR/env/osarch ]; then
    export OS_ARCH=`$DELPHI_INSTALL_DIR/env/osarch`
    export OS_TAG=`$DELPHI_INSTALL_DIR/env/ostag`
    export GROUP_DIR=${DELPHI_INSTALL_DIR}/releases/${OS_TAG}
else
    if [ -e env/osarch ]; then
	export OS_ARCH=`env/osarch`
	export OS_TAG=`env/ostag`
	export GROUP_DIR=${DELPHI_INSTALL_DIR}/releases/${OS_TAG}
    fi
fi

if [ -e $GROUP_DIR/profiles/delphi.sh ]; then
  . $GROUP_DIR/profiles/delphi.sh
  # output news
  if [ -e "/cvmfs/delphi.cern.ch/motd" -a -e "/cvmfs/delphi.cern.ch/news" ]; then
    case $- in
      *i*) cat /cvmfs/delphi.cern.ch/motd /cvmfs/delphi.cern.ch/news
    esac
  fi
else
    echo "This OS (${OS_TAG}) is not supported."
    echo 'If you think this is a mistake, please contact us.'
fi

