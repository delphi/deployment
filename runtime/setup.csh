# general path, defaults to cvmfs

source /cvmfs/delphi.cern.ch/unset.csh
if ( ! $?DELPHI_INSTALL_DIR ) then
    setenv DELPHI_INSTALL_DIR "/cvmfs/delphi.cern.ch"
else if ( "x$DELPHI_INSTALL_DIR" == "x" ) then
    setenv DELPHI_INSTALL_DIR "/cvmfs/delphi.cern.ch"
endif

# 
if ( -e $DELPHI_INSTALL_DIR/env/ostag) then
    setenv OS_TAG `$DELPHI_INSTALL_DIR/env/ostag`
    setenv OS_ARCH `$DELPHI_INSTALL_DIR/env/osarch`
    setenv GROUP_DIR ${DELPHI_INSTALL_DIR}/releases/${OS_TAG}
else if ( -e env/ostag ) then
    setenv OS_TAG `env/ostag`
    setenv OS_ARCH `env/osarch`
    setenv GROUP_DIR ${DELPHI_INSTALL_DIR}/releases/${OS_TAG}
endif

if (-e $GROUP_DIR/profiles/delphi.csh) then
  source $GROUP_DIR/profiles/delphi.csh
  # output news
  if ($?prompt) then
    if ( -e /cvmfs/delphi.cern.ch/motd) then
      cat /cvmfs/delphi.cern.ch/motd
    endif
    if (-e /cvmfs/delphi.cern.ch/news) then
      cat /cvmfs/delphi.cern.ch/news
    endif
  endif
else
  echo "This OS (${OS_TAG}) is not supported by DELPHI"
  echo 'If you think this is a mistake, please contact us.'
endif

