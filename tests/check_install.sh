#!/bin/bash
rc=0;
if [[ ! -z "${GROUP_DIR}" ]]; then
    cd ${GROUP_DIR}
    epawx11=2
    # check Motif CERN apps
    fpawx11=`find . -name pawX11 | wc -l`
    if [ "x$epawx11" = "x$fpawx11" ]; then
	echo "pawX11 has been correctly created"
    else
	echo "pawX11 is missing."
	rc=2
    fi
    epawpp=2
    # check Motif CERN apps
    fpawpp=`find . -name paw++ | wc -l`
    if [ "x$epawpp" = "x$fpawpp" ]; then
	echo "paw++ has been correctly created"
    else
	echo "paw++ is missing."
	rc=2
    fi
    # count testing libraries
    elibs=597
    flibs=`find . -name \*.a | wc -l`
    if [ "x$elibs" = "x$flibs" ]; then
	echo "All expected $elibs libraries have been created"
    else
	echo "Some libraries are missing"
	echo "Expected: $elibs, found $flibs"
	find . -name \*.a 
	rc=2
    fi
    # count exe files
    eexe=58
    fexe=`find . -name \*.exe | wc -l`
    if [[ "x$eexe" = "x$fexe" ]]; then
	echo "All expected $eexe exec files have been created"
    else
	echo "Some exe files are missing"
        echo "Expected: $eexe, found $fexe "
	find . -name \*.exe 
	rc=3
    fi
    simrun36=`find . -name simrun36 -type f| wc -l`
    nexp=22
    if [ "x$simrun36" = "x$nexp" ]; then
	echo "All expected $nexp simrun36 executables are there."
    else
	echo "Problems with simrun36."
	echo "expected $nexp, found $simrun36"
	rc=5
    fi
    delgra=`find . -name delgra -type f| wc -l`
    copa=`find . -name copa -type f| wc -l`
    if [[ "x$delgra" = "x1" &&  "x$copa" = "x1" ]]; then
	echo "Delgra looks good"
    else
	echo "Problems in Delgra."
	echo "delgra: $delgra, copa: $copa"
	rc=4
    fi
else
    echo "GROUP_DIR is not set"
    rc=1
fi
exit $rc
# 
