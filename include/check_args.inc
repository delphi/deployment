if [ "x{$VERS}" == "x" ];then
    echo "you need to specify the dstana version to install"
    exit 1
else
    export VERS
fi

if [ "x{$ARCHIVE}" == "x" ];then
    echo "you need to specify the directory which contains the tar balls as second argument"
    exit 1
else
    export ARCHIVE
fi

if [ "x{$DELPHI_INSTALL_DIR}" == "x" ];then
    export DELPHI_INSTALL_DIR=`pwd`
    echo "$DELPHI_INSTALL_DIR not set. Installing here."
else
    export DELPHI_INSTALL_DIR
fi

# setup environment
. delphi.sh
