#! /usr/bin/perl -w
#
#Change log:
#----------
# FC06/06/03 Correction for DELSIM title card item THEMIN and THEMAX, now chosen
#            according the wanted values for baba and bafo internal generators 
# US28/05/02 96g included
# US24/05/02 few additions for LEP1 long dst processing (if anyone needs it ...)
#            fixed building options acra and scra for LEP1 and LEP2 reprocessing
# US23/05/02 added bbps internal generator
# US30/04/02 changes for 92e gas/liq setup title files
# US13/12/01 added support for shortdst (and longdst) for LEP1 stuff
# US04/12/01 need two different title files for delsim/delana for 97g Z0 and HE. fixed.
# US21/11/01 support for 97g
# US08/11/01 preliminary support for 93d and 92e
# US10/10/01 preliminary support for v95d and v94c
# US12/09/01 dump of scan list for debugging after delana finished
# US18/07/01 fixed ecmlep in delana title card
# US05/07/01 build in generator qqps (jetset 7.2)
# US21/06/01 added support for v98e
# US31/05/01 added support for va0e, va0u and v99e, removed v99d and va0d
# US26/01/01 added support for v00_s
#
#
# useful modules
#
use strict;
use diagnostics;
use File::Basename;       # basename
use POSIX qw(uname);      
use Getopt::Long;         # get options
use Sys::Hostname;        # Try every conceivable way to get hostname
#
# parse and check the options
#
my $STARTTIME = localtime;
my $hostname = hostname;
undef my %SIMANA_OPT;
print "\n\n running Version 1.99\n\n";
#
# define LEP1 years
# 
my @lep1 = qw(95 94 93 92 91 90);
#
# get options
#
GetSimanaOptions();
#
# OS dependent stuff
#
my ($OS,$OS_version) = (uname)[0,2];
my %OS_stuff = ( "OSF1"   => [("ALPHAOSF","dbx","fort.","f77 -g1 -w -V -g -C")],
                 "Linux"  => [("LINUX",   "gdb","fort.","gfortran -mfpmath=387 -fno-f2c -fno-automatic -fno-aligncommons -std=legacy -fno-backslash -O -Wno-globals -g")],
                 "Darwin" => [("DARWIN", "lldb","fort.","gfortran -fno-f2c -fno-automatic -fno-aligncommons -std=legacy -fno-backslash -O -Wno-globals -g")],
                 "HP-UX"  => [("HPUX",    "xdb","ftn", "fort77 +ppu +E1 +E4 -K -w +DA1.1 -V -O2 -g")],
                 "AIX"    => [("IBMRT",   "dbx","fort.","xlf -qextname -O -berok -qxlf77=leadzero -qsource -g -C")],
                 );
my ($PLINAM,$XDB,$FTN,$F77) = @{$OS_stuff{$OS}};
#
# where are the DELSIM executables and data files?
#

my $DELSIM_ROOT = $ENV{DELSIM_ROOT} || $ENV{GROUP_DIR}."/delsim";
my $DELSIM_BIN  = $ENV{DELSIM_BIN}  || "$DELSIM_ROOT/$SIMANA_OPT{VERSION}/bin";
my $DELSIM_LIB  = $ENV{DELSIM_LIB}  || "$DELSIM_ROOT/$SIMANA_OPT{VERSION}/lib";
my $DELSIM_PAM  = $ENV{DELSIM_PAM}  || "$DELSIM_ROOT/$SIMANA_OPT{VERSION}/src/car";
my $DELSIM_DAT  = $ENV{DELSIM_DAT}  || "$DELSIM_ROOT/$SIMANA_OPT{VERSION}/dat";
my $DELSIM_BLKD = $ENV{DELSIM_BLKD} || "$DELSIM_ROOT/$SIMANA_OPT{VERSION}/blkd";


unlink <FOR*>,<ftn*>,<fort*>,<*.f>,<*.o>,<*.l>,<*.lst>;

# DELSIM

RunSimrun() if defined $SIMANA_OPT{SIM};

# DELANA

RunDelana() if defined $SIMANA_OPT{ANA};

# XShortDST production

RunXShortDST() if defined $SIMANA_OPT{XSDST};

# ShortDST production

RunShortDST() if defined $SIMANA_OPT{SDST} && ! defined $SIMANA_OPT{LONG};

# LongDST production

RunLongDST() if defined $SIMANA_OPT{LDST}|| defined $SIMANA_OPT{LONG};

# done

my $STOPTIME = localtime;

print STDOUT <<EOOUT;

*************************************************
***
*** Running on $hostname
*** Started at  $STARTTIME
*** Finished at $STOPTIME
***
*************************************************

EOOUT

exit 0;

#############################################################

# Create title file for SIMRUN

sub MakeSimTitle {
    my $dbfile = "$DELSIM_BLKD/dtbzeb.datwri";
    my $version = $SIMANA_OPT{VERSION};
    my %opt = ( FLIMOU => $SIMANA_OPT{FLIMOU} || "5000.",
                NEVMAX => $SIMANA_OPT{NEVMAX},
                LRNRUN => "TRUE", # Random number from lab+run number ?
                SXFREP => "FALSE",
                JFLREP => 3,
                SXFMIS => "FALSE",
                JQUARK => 0,
                SXFNUC => "TRUE",
                IRUN   => -$SIMANA_OPT{NRUN},
                IEXPNB => -$SIMANA_OPT{IEXPNB},
                ISWIDB => "1 0 0",
                ISEEDG => "-1 0 0",
                ISEEDS => "-1 0 0",
                EBEAM  => $SIMANA_OPT{EBEAM},
                IGENER => $SIMANA_OPT{IGENER},
                NGMAX  => 1,
                MSTJ   => " ",
#                THEMIN => "45.",
#                THEMAX => "135.", 
                THEMIN => $SIMANA_OPT{THEMIN} || "45.",
                THEMAX => $SIMANA_OPT{THEMAX} || "135.",
                LUDECV => $SIMANA_OPT{LUDECV} || "FALSE",
                XMH    => $SIMANA_OPT{XMH},
                JPSEL  => $SIMANA_OPT{JPSEL} || " ",
                JPSELZ => " ",
                JPSELH => " ",
                XMH1   => $SIMANA_OPT{XMH},
                JPSLHP => " ",
                JPSLHM => " ",
                MDB3   => " ",
                #C-- Write (WDBZEB) or Load (LDBZEB) the database to/from unit LUNDBZ
                WDBZEB => ( -e $dbfile ? "FALSE" : "TRUE"  ),
                LDBZEB => ( -e $dbfile ? "TRUE"  : "FALSE" ),
                LUNDBZ => 59,
                IDLUIN => $SIMANA_OPT{IDLUIN},
                LABO   => "'" . $SIMANA_OPT{LABO} . "'",
                DBVIRT => $SIMANA_OPT{VIRT},
               );
    #
    my $MSTPDEV = $SIMANA_OPT{MSTPDEV} || " ";
    my $MSIMDEV = $SIMANA_OPT{MSIMDEV} || "'ID  '";
    #
    if ($opt{LDBZEB} eq "TRUE"){ 
        symlink($dbfile,"${FTN}$opt{LUNDBZ}") or PrintError("cannot link \"$dbfile\": $!");
    }
    #
    # user specified ASCII virtual database file
    #
    if ( $SIMANA_OPT{VIRT} ne "-1 'NONE'" ) {
        unlink "${FTN}$opt{LUNDBZ}";
        ($opt{WDBZEB},$opt{LDBZEB}) = ("TRUE","FALSE");
        symlink($SIMANA_OPT{VIRT},"${FTN}30") or PrintError("cannot link \"$SIMANA_OPT{VIRT}\": $!");
        $SIMANA_OPT{VIRT} = " 30 'NEW' ";
        symlink("dtbzeb.datwri","${FTN}$opt{LUNDBZ}") or PrintError("cannot link \"dtbzeb.datwri\": $!");
    }
    #
    # now regenerate title file
    #
    my $title = "simlocal.title";
    unlink $title if -e $title;
    my %in = (
	"v90e"  => "$DELSIM_DAT/s90t94_liq.tit", # default liq setup
	"v91f"  => "$DELSIM_DAT/s91t94_liq.tit", # default liq setup
	"v92e"  => "$DELSIM_DAT/s92t94_liq.tit", # default liq setup
	"v93d"  => "$DELSIM_DAT/s93t94_liq.tit",
	"v94c"  => "$DELSIM_DAT/simqqbar.tit",
	"v95d"  => "$DELSIM_DAT/simqqbar.tit",
	"v99_3" => "$DELSIM_DAT/qqsim35.titlsh6",
	"v99_4" => "$DELSIM_DAT/qqsim35.titlsh6",
	"v00_s" => "$DELSIM_DAT/qqsim35.titlsh6",
	"v96g"  => "$DELSIM_DAT/simlep200.tit",
	"v97g"  => "$DELSIM_DAT/simlep200.tit",
	"v98e"  => "$DELSIM_DAT/simlep200.tit",
	"v99e"  => "$DELSIM_DAT/simlep200.tit",
	"va0e"  => "$DELSIM_DAT/simlep200.tit",
	"va0z"  => "$DELSIM_DAT/simlep200.tit",
	"va0u"  => "$DELSIM_DAT/simlep200.tit"
	);
    #special treatment for 97g required (sadly, yes)
    PrintError("Cannot make delsim title file for version \"$version\"") if not exists $in{$version};
    my $simtit = $in{$version};
    $simtit = "$DELSIM_DAT/simlep200_z0.tit" if ($version eq "v97g" && $SIMANA_OPT{EBEAM} < 48.0);
    PrintInfo("Running with option $SIMANA_OPT{SETUP}");
    $simtit =~ s/liq/$SIMANA_OPT{SETUP}/; # switch to gas setup if requested by setup option
    open(IN,"<$simtit") or PrintError("cannot open \"$simtit\": $!");
    open(OUT,">$title") or PrintError("cannot open \"$title\": $!");
    while(<IN>){
        next if /\+titlefin/;
        /^([\d\w]+)\s+.*$/ && do {$_ = "$1 $opt{$1}\n" if defined $opt{$1}};
        s/^(VTAU 125=).*$/$1$SIMANA_OPT{TAUB}/;
        s/^(VTAU 126=).*$/$1$SIMANA_OPT{TAUB}/;
        s/^(VTAU 127=).*$/$1$SIMANA_OPT{TAUB}/;
        s/^\s*IDTOUT\s*.*$/IDTOUT    7/;
        s/^\s*ITOUT\s*.*$/ITOUT    20/;
        s/^\s*LOUTAN\s*.*$/LOUTAN   20/;
        s/\$mstpdev/$MSTPDEV/g;
        s/\$msimdev/$MSIMDEV/g;
        print OUT;
    }
    close(IN);
    close(OUT);
    #
    # append year dependent stuff
    #
    if ($version =~ /^v98_/) {
        System("cat $DELSIM_DAT/ddmlgc98_11LIQ.dat >> $title");
    }elsif ($version =~ /^v99_/) {
        System("cat $DELSIM_DAT/ddmlgc99_11LIQ.dat >> $title");
    }elsif ($version eq "v00_s") { #US
        System("cat $DELSIM_DAT/ddmlgc99_11LIQ.dat >> $title");
    #}else{
    #    PrintError("Not yet implemented, complain to Ulrich.Schwickerath\@cern.ch");
    }
    #system("cat $title");exit;
    return;
}

#############################################################

# wrap the system command

sub System {
    for my $command (@_){
        if ( $command =~ /^warning/i ) {
            print STDOUT "## ".uc($command)." ##\n";
            next;
        }
        print STDOUT "## SYS  :: $command\n";
        system($command);
        #qx($command);
        PrintError("\"$command\" fails:",$!) if $?;
    }
    return;
}

#############################################################

# make SIMRUN executable

sub MakeSimrun {
    my $cradle = shift;
    
    my $lib = "simrun36";
    unlink $lib;
    my @liblist;
    (grep {$SIMANA_OPT{VERSION} =~ /$_/} @lep1) >0 ? do {@liblist = qw(delsim36 modsim36 delsim36 jetst732 ufield ddappxx tanagra322)} : do {@liblist = qw(delsim modsim delsim jetst732 ufield ddappxx tanagra322)};
    for my $dlib (@liblist) {
        PrintError("Cannot create \"$lib\": no library $DELSIM_LIB/lib$dlib.a") if not -f "$DELSIM_LIB/lib$dlib.a";
    }
    map {$_ = "-l$_"} @liblist; # prepend each library with "-l"
    System("cp $cradle $lib.cra");
    map {$ENV{$_} = $DELSIM_PAM} qw(DELANA_ANA DELANA_PHY DELANA_UTY DELANA_TAN);
    map {$ENV{$_} = $DELSIM_PAM} qw(ANA99C PHY99C UTY99C TAN99C);
    map {$ENV{$_} = $DELSIM_PAM} qw(SIMUTY SIMDIR SIMTAN SIMPHY SIMCAR);
    map {$ENV{$_} = $DELSIM_PAM} qw(SIMCAR ANACAR);
    map {$ENV{$_} = $DELSIM_PAM} qw(ANADIR PHYDIR TANDIR UTYDIR);
    System("nypatchy - $lib.fca $lib $lib.lis .go",      # patchy
           "fcasplit $lib.fca",                          # fcasplit
           );
    #my $dellib = "-L$DELSIM_LIB ".join(" ",map {sprintf("-l%s",$_)} @liblist);
    my $dellib = "-L$DELSIM_LIB @liblist";
    #my $cernlib = `cernlib photos pdflib eurodec ariadne pawlib mathlib graflib/Motif packlib pawlib mathlib kernlib`;
    chomp (my $cernlib = `cernlib photos pdflib eurodec ariadne pawlib mathlib graflib/Motif packlib pawlib mathlib kernlib`); # with Motif
    if ( $OS eq "Linux" || $OS eq "Darwin" ) { # do not link to shared libraries
	my $liblist = "/usr/X11R6/lib/libXm.a";
	$liblist = "/usr/local/lib/libXm.a" if -f "/usr/local/lib/libXm.a"; # Lesstif on RH6
	$liblist = "/usr/lib/libXm.a"       if -f "/usr/lib/libXm.a";       # Lesstif on Debian
	map {$liblist .= " /usr/X11R6/lib/lib$_.a"} qw(Xt SM ICE X11);
	$cernlib =~ s|-L/usr/X11R6/lib .*-lX11|$liblist|;
	map {$cernlib =~ s| -l$_| /usr/lib/lib$_.a|} qw(nsl crypt dl);
    }
    $cernlib =~ s|libpdflib|libpdflib804| if -e "$ENV{CERN_LIB}/libpdflib804.a" and not -e "$ENV{CERN_LIB}/libpdflib.a";
    #chomp (my $cernlib = `cernlib photos  eurodec ariadne pawlib mathlib packlib pawlib mathlib kernlib`);
    #$F77 = "xlf -qextname -O" if $OS eq "AIX";
    my $command = "$F77 -o $lib *.f $DELSIM_BLKD/simblkd.o $dellib $cernlib";
    #chop(my $command = "$F77 -o $lib *.f $dellib $cernlib");
    PrintInfo("executing \"$command\"");
    system("$command");
    PrintError("No executable \"$lib\" created") if not -f $lib;
    chmod 0755,$lib;
    return "./$lib";
}

#############################################################

# Run SIMRUN executable

sub RunSimrun {
    # get/create title file
    my $starttime = localtime;
    if (defined $SIMANA_OPT{STITL} ){
        my $simtitle = $SIMANA_OPT{STITL};
        system("/bin/cp $simtitle simlocal.title") and PrintError("cannot copy \"$simtitle\": $!");
        # bug fix (but veeeerrrrryyyy ugly!)    JvE, Feb 2001
        my $dbfile = "$DELSIM_BLKD/dtbzeb.datwri";
        symlink($dbfile,"${FTN}59") or PrintError("cannot link \"$dbfile\": $!");
    }else{
        MakeSimTitle();
    }
    # create necessary links
    DBlinks(); # setup links to the snapshot database
    my $simdec = $SIMANA_OPT{SIMDEC} || "$DELSIM_BLKD/simdec.data";
    my %SymLinks = ("${FTN}16" => "$DELSIM_DAT/pegsdat.data",
                    "${FTN}79" => "$DELSIM_DAT/fcabtrig.logics",
                    "${FTN}78" => "$DELSIM_DAT/fcabtrig.window",
                    "${FTN}03" => "delsimrn.out3",
                    "${FTN}88" => "delsimrn.out88",
                    "${FTN}39" => "rndm.store",
                    "${FTN}58" => "igtots.logn",
                    "${FTN}20" => $SIMANA_OPT{RAWFILE},
                    "${FTN}01" => $simdec,
                    "${FTN}1"  => $simdec,
                    "${FTN}15" => "simlocal.title",
                    );
    map{unlink $_ ; symlink($SymLinks{$_},$_) or PrintError("cannot link \"$SymLinks{$_}\": $!") } keys %SymLinks;
    #
    if (defined $SIMANA_OPT{GEXT}){
        for my $ext (@{$SIMANA_OPT{GEXT}}){
            if ( $ext =~ /^([A-Z0-9]{6})\.(\d+)$/i and not -e $ext){
                PrintInfo("Retrieving external tape file \"$ext\"");
                if ( not -e "/tmp/$ext"){
                    System("delstage -f $2 $1");
                    System("rfcp T$1.FSEQ$2 /tmp/$ext");;
                }
                $ext = "/tmp/$ext";
                #symlink "/tmp/$ext",$ext;
            }elsif ( $ext =~ /^\/castor|^hpss/){
                my $hsmfile = $ext;
                $ext = basename $ext;
                PrintInfo("Retrieving external HSM file \"$hsmfile\"");
                if ( not -e "/tmp/$ext"){
                    System("delstage -M $hsmfile");
                    System("rfcp $ext /tmp/$ext");
                    unlink $ext;
                    #symlink "/tmp/$ext",$ext;
                }
                $ext = "/tmp/$ext";
             #}else{
            #    symlink($ext,"${FTN}$SIMANA_OPT{IDLUIN}") or PrintError("cannot link \"$_\": $!");
            }
            symlink($ext,"${FTN}$SIMANA_OPT{IDLUIN}") or PrintError("cannot link \"$_\": $!");
        }
    }

    undef my $simrun;
    if (defined $SIMANA_OPT{SCRA}){
        $simrun = MakeSimrun($SIMANA_OPT{SCRA});
    }else{
        $simrun = "simrun36";
        System("/bin/cp $DELSIM_BIN/simrun36 $simrun");
        chmod 0755,$simrun;
    }
    unlink $SIMANA_OPT{RAWFILE};
    PrintInfo("running DELSIM $simrun");
    #my $cmd = "totalview ./$simrun 2>&1";
    my $cmd = "./$simrun 2>&1";
    system($cmd);
    my $stoptime = localtime;
    PrintInfo(" ","RunSimrun:: started  $starttime",
                  "RunSimrun:: finished $stoptime","");
    return;
}


#############################################################

# make DELANA executable

sub MakeDelana {
  my $cradle = shift;
  
  my ($lib,@liblist);
  (grep {$SIMANA_OPT{VERSION} =~ /$_/} @lep1) >0 ? do {$lib="delana43";@liblist = qw(trigglib pxdst33 pxtag25 pxdst33 delphys deltask delanamod delanagen delanamod ux26 ddappxx tanagra322 uhlib ufield com)} : do {$lib="delana45";@liblist = qw(delsim modsim delsim jetst732 ufield ddappxx tanagra322)};
  unlink $lib;
  for my $dlib (@liblist) {
    PrintError("Cannot create \"$lib\": no library $DELSIM_LIB/lib$dlib.a") if not -f "$DELSIM_LIB/lib$dlib.a";
  }
  map {$_ = "-l$_"} @liblist; # prepend each library with "-l"
  System("cp $cradle $lib.cra");
  map {$ENV{$_} = $DELSIM_PAM} qw(DELANA_ANA DELANA_PHY DELANA_UTY DELANA_TAN);
  map {$ENV{$_} = $DELSIM_PAM} qw(ANA99C PHY99C UTY99C TAN99C);
  map {$ENV{$_} = $DELSIM_PAM} qw(SIMUTY SIMDIR SIMTAN SIMPHY SIMCAR);
  map {$ENV{$_} = $DELSIM_PAM} qw(SIMCAR ANACAR);
  map {$ENV{$_} = $DELSIM_PAM} qw(ANADIR PHYDIR TANDIR UTYDIR);
  System("nypatchy - $lib.fca $lib $lib.lis .go",      # patchy
	  "fcasplit $lib.fca",                          # fcasplit
       );
  #my $dellib = "$DELSIM_LIB/fxsolv.o -L$DELSIM_LIB ".join(" ",map {sprintf("-l%s",$_)} @liblist);
  my $dellib = "$DELSIM_LIB/fxsolv.o -L$DELSIM_LIB @liblist";
  chomp (my $cernlib = `cernlib genlib kernlib graflib packlib mathlib jetset74`);
  #$F77 = "xlf -qextname -O" if $OS eq "AIX";
  my $command = "$F77 -o $lib *.f $DELSIM_BLKD/anablkd.o $dellib $cernlib";
  #chop(my $command = "$F77 -o $lib *.f $dellib $cernlib");
  PrintInfo("executing \"$command\"");
  system("$command");
  PrintError("No executable \"$lib\" created") if not -f $lib;
  chmod 0755,$lib;
  return "./$lib";
}


#############################################################

# Run DELANA executable

sub RunDelana {
    my $starttime = localtime;
    #
    # make sure the input file is ready for us
    #
    my $version = $SIMANA_OPT{VERSION};    
    my %delanav = (
	"v90e"  => "delana43.exe",
	"v91f"  => "delana43.exe",
	"v92e"  => "delana43.exe",
	"v93d"  => "delana43.exe",
	"v94c"  => "delana43.exe",
	"v95d"  => "delana43.exe",
	"v99_3" => "delana45.exe",
	"v99_4" => "delana45.exe",
	"v00_s" => "delana45.exe",
	"v96g"  => "delana45.exe",
	"v97g"  => "delana45.exe",
	"v98e"  => "delana45.exe",
	"v99e"  => "delana45.exe",
	"va0e"  => "delana45.exe",
	"va0z"  => "delana45.exe",
	"va0u"  => "delana45.exe"
	);
    for my $ext ($SIMANA_OPT{RAWFILE}){
        if ( $ext =~ /^([A-Z0-9]{6})\.(\d+)$/i and not -e $ext){
            PrintInfo("Retrieving external tape file \"$ext\"");
            System("delstage -f $2 $1");
            System("rfcp T$1.FSEQ$2 /tmp/$ext") if not -e $ext;
            symlink "/tmp/$ext",$ext;
        }elsif ( $ext =~ /^\/castor/){
            my $hsmfile = $ext;
            $ext = basename $ext;
            PrintInfo("Retrieving external HSM file \"$hsmfile\"");
            if ( not -e "/tmp/$ext"){
                System("delstage -M $hsmfile");
                System("rfcp $ext /tmp/$ext");
            }
            unlink $ext;
            symlink "/tmp/$ext",$ext;
            $SIMANA_OPT{RAWFILE} = $ext;
        }
    }
    $ENV{dinp01} = $SIMANA_OPT{RAWFILE};
    $ENV{dout01} = $SIMANA_OPT{DSTFILE};
    unlink <FOR*>,<ftn*>,<fort*>,<*.f>,<*.o>,<*.l>,<*.lst>;
    #
    # prepare the title file
    #
    my $out = "delana.tit";
    my $in;
    if (defined $SIMANA_OPT{ATITL}){
	$in = $SIMANA_OPT{ATITL}
    } else {
	$in  = "$DELSIM_DAT/delana.tit";
	$in  = "$DELSIM_DAT/delana_z0.tit" if ($version eq "v97g" && $SIMANA_OPT{EBEAM} < 48.0);
    }
    PrintInfo("using delana title file $in");
    open(IN, "<$in")  or PrintError("could not open \"$in\"");
    open(OUT,">$out") or PrintError("could not open \"$out\"");
    my $ecms = 2.*$SIMANA_OPT{EBEAM};
    while(<IN>){
        s/LABO.*/LABO '$SIMANA_OPT{LABO}'/;
        s/NEVMAX.*/NEVMAX $SIMANA_OPT{NEVMAX}/;
        s/MBLIMI.*/MBLIMI  5000./;      # 5000 Mb output file
	s/ECMLEP.*/ECMLEP $ecms/;
        print OUT $_;
    }
    close(IN);
    close(OUT);
    DBlinks(); # setup links to the snapshot database
    my %SymLinks = ("${FTN}29" => "delana.tit",
                    "${FTN}06" => "delanarn.out6",
                    "${FTN}92" => "detector.sumr",
                    "${FTN}43" => "scanlist.sumr",
                    "${FTN}44" => "survey.sumr",
                    "${FTN}42" => "alignmnt.sumr",
                    "${FTN}60" => "hbookout.sumr",
                    "${FTN}95" => "delanarn.ou95",
                    "${FTN}99" => "delanarn.ou99",
                    "${FTN}58" => "nevtot.logn",
                    );
    for (keys %SymLinks){
        unlink $_;
        symlink($SymLinks{$_},$_) or PrintError("cannot link $SymLinks{$_}: $!");
    }
    #exit;
    symlink($SIMANA_OPT{DSTFILE},"fort.21") if $OS eq "AIX"; # CJAN UX incompatibility
    unlink "${FTN}20",$SIMANA_OPT{DSTFILE};
#    symlink("$DELSIM_DAT/tof_fit_92.asc","${FTN}30") if -e "$DELSIM_DAT/tof_fit_92.asc";

    undef my $delana;
    if (defined $SIMANA_OPT{ACRA}){
        $delana = MakeDelana($SIMANA_OPT{ACRA});
    }else{
        #$delana = "delana45.exe";
	$delana = $delanav{$version};
        System("/bin/cp $DELSIM_BIN/$delana $delana");
        chmod 0755,$delana;
    }
    system("cat fort.30") if -e "fort.30";
    PrintInfo("running DELANA $delana");#exit;
    my $cmd = "./$delana 2>&1";
    system($cmd);#exit;
    system("cat scanlist.sumr");
    PrintInfo("dumping scan list:");
    unlink <FOR*>,<fort.*>,<ftn*>;
    #unlink $delana;
    my $stoptime = localtime;
    PrintInfo(" ","RunDelana:: started  $starttime",
                  "RunDelana:: finished $stoptime","");
    return;
}

#############################################################

# Setup links to the snapshot database

sub DBlinks {
    my $DBdir = $DELSIM_DAT;
    my %SymLinks = ("FOR050" => "$DBdir/FOR050",
                    "FOR051" => "$DBdir/FOR051",
                    "FOR052" => "$DBdir/FOR052",
                    "FOR053" => "$DBdir/FOR053",
                    "FOR054" => "$DBdir/FOR054",
                    "FOR055" => "$DBdir/FOR055",
                    "FOR056" => "$DBdir/FOR056",
                    );
    map{unlink $_ ; symlink($SymLinks{$_},$_) or PrintError("cannot link $SymLinks{$_}: $!") } keys %SymLinks;
}

#############################################################

# Produce an (x)sdst file

sub RunXShortDST {
    my $starttime = localtime;
    my $version = $SIMANA_OPT{VERSION};
    my $xshort = "xshortdst.exe";
    if (not -e "$DELSIM_BIN/$xshort") {
        PrintInfo("No executable \"$DELSIM_BIN/$xshort\" found, sorry...");
        return;
    }
    #
    unlink $xshort,"PDLINPUT","DESCRIP";
    $ENV{DELPHI_DAT} = $DELSIM_DAT;
    System("/bin/cp $DELSIM_DAT/DESCRIP DESCRIP");
    open(PDL,">PDLINPUT") or PrintError( "Cannot open PDLINPUT: $!");
    map {print PDL "FILE = $_\n"} split(/\s+/,$SIMANA_OPT{DSTFILE}); # ugly!!!
    #print PDL "FILE = $SIMANA_OPT{DSTFILE}\n";
    print PDL "FILE = $SIMANA_OPT{XSDSTFILE}, STREAM = 1, COMPRESS=X\n";
    close(PDL);
    #System("/bin/cp $DELSIM_BIN/xshortdst.exe $xshort");
    System("/bin/cp $DELSIM_BIN/$xshort $xshort");
    chmod 0755,$xshort;
    #exit 1 if ($xshort eq "shortdst.exe"); 
    System("./$xshort");
    unlink $xshort,"PDLINPUT","DESCRIP",<fort.*>,<ftn*>;
    my $stoptime = localtime;
    PrintInfo(" ","RunShortDST:: started  $starttime",
                  "RunShortDST:: finished $stoptime","");
    return;
}

sub RunShortDST {
    my $starttime = localtime;
    my $version = $SIMANA_OPT{VERSION};
    my %shortv = (
	"v90e"  => "shortdst.exe",
	"v91f"  => "shortdst.exe",
	"v92e"  => "shortdst.exe",
	"v93d"  => "shortdst.exe",
	"v94c"  => "shortdst.exe",
	"v95d"  => "shortdst.exe",
	);
    my $short = "shortdst.exe";
    if (not -e "$DELSIM_BIN/$short") {
        PrintInfo("No executable \"$DELSIM_BIN/$short\" found, sorry...");
        return;
    }
    #
    unlink $short,"PDLINPUT","DESCRIP";
    $ENV{DELPHI_DAT} = $DELSIM_DAT;
    System("/bin/cp $DELSIM_DAT/shortdst.des DESCRIP");
    open(PDL,">PDLINPUT") or PrintError( "Cannot open PDLINPUT: $!");
    map {print PDL "FILE = $_\n"} split(/\s+/,$SIMANA_OPT{DSTFILE}); # ugly!!!
    #print PDL "FILE = $SIMANA_OPT{DSTFILE}\n";
    print PDL "FILE = $SIMANA_OPT{SDSTFILE}, STREAM = 1, COMPRESS=X\n";
    close(PDL);
    System("/bin/cp $DELSIM_BIN/$short $short");
    chmod 0755,$short;
    System("./$short");
    unlink $short,"PDLINPUT","DESCRIP",<fort.*>,<ftn*>;
    my $stoptime = localtime;
    PrintInfo(" ","RunShortDST:: started  $starttime",
                  "RunShortDST:: finished $stoptime","");
    return;
}

sub RunLongDST {
    my $starttime = localtime;
    my $version = $SIMANA_OPT{VERSION};
    my $short = "longdst.exe";
    if (not -e "$DELSIM_BIN/$short") {
        PrintInfo("No executable \"$DELSIM_BIN/$short\" found, sorry...");
        return;
    }
    #
    unlink $short,"PDLINPUT","DESCRIP";
    $ENV{DELPHI_DAT} = $DELSIM_DAT;
    System("/bin/cp $DELSIM_DAT/longdst.des DESCRIP");
    open(PDL,">PDLINPUT") or PrintError( "Cannot open PDLINPUT: $!");
    map {print PDL "FILE = $_\n"} split(/\s+/,$SIMANA_OPT{DSTFILE}); # ugly!!!
    print PDL "FILE = $SIMANA_OPT{LDSTFILE}, STREAM = 1, COMPRESS=X\n";
    close(PDL);
    System("/bin/cp $DELSIM_BIN/$short $short");
    chmod 0755,$short;
    System("./$short");
    unlink $short,"PDLINPUT","DESCRIP",<fort.*>,<ftn*>;
    my $stoptime = localtime;
    PrintInfo(" ","RunShortDST:: started  $starttime",
                  "RunShortDST:: finished $stoptime","");
    return;
}

#############################################################

# Get lab identifiers (from UPLABO in delsim36.car)

sub LabInfo {
    my $name = shift;
    my %Labs = ( 'WIEN' => 110,
                 'BELG' => 200,
                 'NBI ' => 310,
                 'HELS' => 410,
                 'CDF ' => 510,
                 'LAL ' => 520,
                 'LPNH' => 530,
                 'SACL' => 540,
                 'STRA' => 550,
                 'KARL' => 610,
                 'WUPP' => 620,
                 'LIVE' => 710,
                 'OXFO' => 720,
                 'RAL ' => 730,
                 'ATHE' => 810,
                 'ANTU' => 820,
                 'DEMO' => 830,
                 'BOLO' => 910,
                 'GENO' => 920,
                 'MILA' => 930,
                 'PADU' => 940,
                 'ROMA' => 950,
                 'TRIE' => 960,
                 'TORI' => 970,
                 'NIKH' => 1010,
                 'BERG' => 1110,
                 'OSLO' => 1120,
                 'CRAC' => 1210,
                 'WARS' => 1220,
                 'SANT' => 1310,
                 'VALE' => 1320,
                 'LUND' => 1410,
                 'STOC' => 1420,
                 'UPPS' => 1430,
                 'AMES' => 1510,
                 'SERP' => 1610,
                 'DUBN' => 1620,
                 'LISB' => 1710,
                 'CERN' => 2000,
                 'FARM' => 1990,
                 'SNAK' => 1980,
                 'LYON' => 560,
                 'GREN' => 570,
                 'MARS' => 580,
                 'CCPN' => 500,
                 'BAST' => 590,
                 'LJUB' => 2110,
                 );
    PrintError("Unknown lab name \"$name\"") if not defined $Labs{$name};
    return $Labs{$name};
}

#############################################################

# Print info

sub PrintInfo {
    my @string = @_;
    return unless @string;
    my $mess = "## INFO :: ";
    print STDOUT "$mess".join("\n$mess",@string)."\n";
}

#############################################################

# Print error

sub PrintError {
    my @string = @_;
    return unless @string;
    my $mess = "## ERROR :: ";
    print STDERR "$mess\n$mess".join("\n$mess",@string)."\n$mess\n";
    exit 1;
}

#############################################################

# Parse the user options

sub GetSimanaOptions {
    #
    # Welcome the user
    # 
    print STDOUT <<EOUT;

*************************************************
*************************************************

  ######  ####### #        #####    ###   #     #
  #     # #       #       #     #    #    ##   ##
  #     # #       #       #          #    # # # #
  #     # #####   #        #####     #    #  #  #
  #     # #       #             #    #    #     #
  #     # #       #       #     #    #    #     #
  ######  ####### #######  #####    ###   #     #

          the DELPHI Simulation package

*************************************************
*************************************************
***
*** Submitted with options:
***
***    \"@ARGV\"
***
EOUT
    #
    # default values
    #
    $SIMANA_OPT{VIRT} = "-1 'NONE'";
    $SIMANA_OPT{XMH}  = "10.";
    $SIMANA_OPT{TAUB} = "1.6";
    $SIMANA_OPT{NEVMAX} = 1000000;
    #
    # parse the options
    #
    my $status = Getopt::Long::GetOptions
        (\%SIMANA_OPT,
         # technical options
         "NRUN=i",
         "WDBZ",
         "DST=s",
         "NOWSIM",
         "WANA",
         "RNDM",
         "NEVMAX=i", # CJAN new option
         "MODEL=s",  # CJAN new option
         "LABO=s",  # CJAN new option
         "FLIMOU=f", # max size of Simana output file
         "VERSION=s",
         "SETUP=s",  # CUS new option
         #"SEED=???",
         "REPT=i",
         # physical parameters
         "EBEA=f",
         "EBEAM=f",
         "TAUB=f",
         "XMH=f",
         "FILT=s",
         "GEXT=s@",
         "FEXT=s",
         "DEXT=s",
         "LEXT=s",
         # testing
         "SIM",
         "XSDST",
         "STST",
         "ATST",
         # Dstana processing
	 "LDST",   #CUS new option
	 "LONG",   #CUS new option
         # Delana processing
         "LANA=s",
         "ANA",
         "RANA",
         "FSEQ=s",
         # title files
         "STITL=s",
         "SIMDEC=s",
         "ATITL=s",
         "STITA=s",
         "ATITA=s",
         "SSTRG=s",
         "ASTRG=s",
         "LUDECV=s",  # CUS new option
         # to recompile and/or test
         "SCRA=s", # user cradle for simulation
         "ACRA=s", # user cradle for delana
         "VIRT=s", #
         "MAP",
         # internal generator
         "IGEN=s",
        );
    PrintError("options could not be parsed") unless $status;
    my @mandatory_options = qw(VERSION LABO);
    $SIMANA_OPT{TAUB} .= "E-12";
    $SIMANA_OPT{EBEAM} = $SIMANA_OPT{EBEA} if defined $SIMANA_OPT{EBEA} and not defined $SIMANA_OPT{EBEAM};
    #
    if (defined $SIMANA_OPT{IGEN}){
        my $igen = $SIMANA_OPT{IGEN};
        if ($igen eq "baba"){
            ($SIMANA_OPT{THEMIN},$SIMANA_OPT{THEMAX}) = ("37.","143.");
            $SIMANA_OPT{IGENER} = 14;
            $SIMANA_OPT{JPSEL}  = "0 1 0 0 0 0 0 0 0 0 0 0";
        }elsif ($igen eq "bafo"){
            ($SIMANA_OPT{THEMIN},$SIMANA_OPT{THEMAX}) = ("9.","171.");
            $SIMANA_OPT{IGENER} = 14;
            $SIMANA_OPT{JPSEL}  = "0 1 0 0 0 0 0 0 0 0 0 0";
        }elsif ($igen eq "nunu"){
            $SIMANA_OPT{IGENER} = 14;
            $SIMANA_OPT{JPSEL}  = "1 0 0 0 0 0 0 0 0 0 0 0";
        }elsif ($igen eq "qqps"){
            $SIMANA_OPT{IGENER} = 15;
            $SIMANA_OPT{JPSEL}  = "0 0 1 1 0 0 1 1 0 0 0 1";
        }elsif ($igen eq "bbps"){
            $SIMANA_OPT{IGENER} = 15;
            $SIMANA_OPT{JPSEL}  = "0 0 0 0 0 0 0 0 0 0 0 1";
        }elsif ($igen eq "single"){
            $SIMANA_OPT{IGENER} = 5;
            $SIMANA_OPT{ISWIDB} = "1 2 2";
            $SIMANA_OPT{MSTPDEV} = "'ID  '";
            $SIMANA_OPT{MSIMDEV} = "      ";
            $SIMANA_OPT{MDB3VD} = "'VD  '";
        }elsif ($igen eq "show_nonuc"){
            $SIMANA_OPT{SXFNUC} = "'FALSE'";
        }elsif ($igen eq "mumu"){
            $SIMANA_OPT{IGENER} = 14;
            $SIMANA_OPT{JPSEL}  = "0 0 0 0 0 1 0 0 0 0 0 0";
        }elsif ($igen eq "toto"){
            $SIMANA_OPT{IGENER} = 14;
            $SIMANA_OPT{JPSEL}  = "0 0 0 0 0 0 0 0 0 1 0 0";
        }elsif ($igen eq "toto_nonuc"){
            $SIMANA_OPT{IGENER} = 14;
            $SIMANA_OPT{JPSEL}  = "0 0 0 0 0 0 0 0 0 1 0 0";
            $SIMANA_OPT{SXFNUC} = "'FALSE'";
        }else{
            PrintError("Unknown generator \"$igen\" requested");
        }
        $SIMANA_OPT{IDLUIN} = 0;
    }else{
        $SIMANA_OPT{IGENER} = 0;
        $SIMANA_OPT{IDLUIN} = 18;
        push(@mandatory_options,qw(GEXT));
    }
    #
    # default: SIM + ANA + XSDST
    if (not defined $SIMANA_OPT{SIM} and not defined $SIMANA_OPT{ANA} and not defined $SIMANA_OPT{XSDST} and not defined $SIMANA_OPT{SDST} and not defined $SIMANA_OPT{LDST}){
        $SIMANA_OPT{RAWFILE}   = "simana.fadsim";
        $SIMANA_OPT{DSTFILE}   = "simana.fadana";
        $SIMANA_OPT{XSDSTFILE} = "simana.xsdst";
        $SIMANA_OPT{SDSTFILE}  = "simana.sdst";
        $SIMANA_OPT{LDSTFILE}  = "simana.ldst";
        $SIMANA_OPT{SIM} = $SIMANA_OPT{ANA} = 1;
	$SIMANA_OPT{VERSION} =~ s/\s+//g;  # remove any white space characters
	PrintError("long dst processing not defined for LEP2 versions!") if (defined $SIMANA_OPT{LONG} &&  (grep {$SIMANA_OPT{VERSION} =~ /$_/} @lep1) ==0 );
        (grep {$SIMANA_OPT{VERSION} =~ /$_/} @lep1) >0 ? $SIMANA_OPT{SDST} = 1  :  $SIMANA_OPT{XSDST} = 1;  
    }else{
        # SIM
        if (defined $SIMANA_OPT{SIM}){
            $SIMANA_OPT{RAWFILE} = "simana.fadsim";
        }
        # ANA
        if (defined $SIMANA_OPT{ANA}){
            if (not $SIMANA_OPT{SIM}){
                PrintError("Option \"-GEXT rawdatafile\" is mandatory for running with \"-ANA\"") if not $SIMANA_OPT{GEXT};
                $SIMANA_OPT{RAWFILE} = @{$SIMANA_OPT{GEXT}}[0];
            }
            $SIMANA_OPT{DSTFILE} = "simana.fadana";
        }
        # XSDST 
        if (defined $SIMANA_OPT{XSDST}){
	    PrintError("Option \"-GEXT fulldatafile\" is mandatory for running with \"-XSDST\"") if not $SIMANA_OPT{GEXT};
            $SIMANA_OPT{DSTFILE} = "@{$SIMANA_OPT{GEXT}}"if not defined $SIMANA_OPT{DSTFILE}; # ugly!!!
            $SIMANA_OPT{XSDSTFILE} = "simana.xsdst";
        }
        # SDST 
        if (defined $SIMANA_OPT{SDST}){
	    PrintError("Option \"-GEXT fulldatafile\" is mandatory for running with \"-SDST\"") if not $SIMANA_OPT{GEXT};
            $SIMANA_OPT{DSTFILE} = "@{$SIMANA_OPT{GEXT}}"if not defined $SIMANA_OPT{DSTFILE}; # ugly!!!
            $SIMANA_OPT{SDSTFILE} = "simana.sdst";
        }
        # LONGDST 
        if (defined $SIMANA_OPT{LDST}){
	    PrintError("Option \"-GEXT fulldatafile\" is mandatory for running with \"-LSDST\"") if not $SIMANA_OPT{GEXT};
            $SIMANA_OPT{DSTFILE} = "@{$SIMANA_OPT{GEXT}}"if not defined $SIMANA_OPT{DSTFILE}; # ugly!!!
            $SIMANA_OPT{LDSTFILE} = "simana.ldst";
        }
    }
    #
    push(@mandatory_options,qw(NRUN EBEAM)) if defined $SIMANA_OPT{SIM};
    push(@mandatory_options,qw(GEXT)) if defined $SIMANA_OPT{ANA} and not $SIMANA_OPT{IGEN};
    #print "mand @mandatory_options\n";
    while (my $opt = shift(@mandatory_options)){
        #next if $opt eq "GEXT";
        PrintError("Mandatory option \"-$opt\" not defined") if not defined $SIMANA_OPT{$opt} and not grep {$_ eq $opt} @mandatory_options;
    }
    #
    # check some values
    #
    if (defined $SIMANA_OPT{NRUN}){
        PrintError("specified run number \"$SIMANA_OPT{NRUN}\" not a positive integer") if $SIMANA_OPT{NRUN} !~ /^\d+$/;
    }
    PrintError("requested number of events \"$SIMANA_OPT{NEVMAX}\" not an integer") if $SIMANA_OPT{NEVMAX} !~ /^\d+$/;
#    }
    #map{printf STDOUT "  %-10s = \"%s\"\n",$_,$SIMANA_OPT{$_}} sort keys %SIMANA_OPT;
    $SIMANA_OPT{SETUP} = "liq" if not defined $SIMANA_OPT{SETUP};
    #
    # lab name
    #
    $SIMANA_OPT{LABO} = sprintf("%-4s",uc($SIMANA_OPT{LABO}));
    $SIMANA_OPT{IEXPNB} = LabInfo($SIMANA_OPT{LABO});
    #
    # Welcome the user
    # 
    print STDOUT <<EOUT;
***
*** General options:
***
***    version       : $SIMANA_OPT{VERSION}
***    lab identifier: $SIMANA_OPT{LABO}
***
EOUT
    if (defined $SIMANA_OPT{SIM}){
        print STDOUT "*** Options for Delsim simulation:\n***\n";
        print STDOUT "***    external file       :  @{$SIMANA_OPT{GEXT}}\n" if defined $SIMANA_OPT{GEXT};
        print STDOUT "***    internal generator  :  $SIMANA_OPT{IGEN}\n" if defined $SIMANA_OPT{IGEN};
        print STDOUT "***    run number          :  $SIMANA_OPT{NRUN}\n";
        print STDOUT "***    number of events    :  $SIMANA_OPT{NEVMAX}\n" if $SIMANA_OPT{NEVMAX} < 1000000;
        print STDOUT "***    delsim cradle       :  $SIMANA_OPT{SCRA}\n" if defined $SIMANA_OPT{SCRA};
        print STDOUT "***\n";
    }
    if (defined $SIMANA_OPT{ANA}){
        print STDOUT "*** Options for Delana reconstruction:\n***\n";
        print STDOUT "***    delana cradle       :  $SIMANA_OPT{ACRA}\n" if defined $SIMANA_OPT{ACRA};
        print STDOUT "***    delana title        :  $SIMANA_OPT{ATITL}\n" if defined $SIMANA_OPT{ATITL};
        print STDOUT "***    input Raw data file :  @{$SIMANA_OPT{GEXT}}\n" if defined $SIMANA_OPT{GEXT};
        print STDOUT "***\n";
    }
    print STDOUT <<EOUT;
*************************************************
***
*** Running on $hostname
*** Started at $STARTTIME
***
*************************************************

EOUT
}
