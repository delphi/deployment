#!/usr/bin/perl -w
use strict;
use diagnostics;
print "#INFO Running $0 $ARGV[0]\n";

my $double = 'r';
my $name = 'undef';
my $InFile = $ARGV[0];
my $OutFile = "${InFile}.new";
print("#INFO: processing $InFile\n");
open(OUT,">$OutFile");
open(IN,"<$InFile") or print("cannot open \"$InFile\": $!");
while (<IN>){
    # remove trailing spaces
    chomp;
    # replace tabs by 6 spaces
    s/\t/      /;
    my $orig = $_;
    my $line = $_;
    if ($orig =~ /^\s*$/) {
	print("#INFO Keeping empty line $orig\n");
	$line = "";
    } else {
	# checking for double precision
	if ($line =~ /^\s+IMPLICIT\s+DOUBLE\s+PRECISION/i){
	    print("#INFO: Switching to double precision for function $name\n");
	    $double = 'd';
	}
	if ($line =~ /^\s+SUBROUTINE\s+(\w+)/i){
	    $name = $1;
	    $double = 'r';
	};
	$double = 'r' if ($line =~/^\s+END/i);
	if ($line =~ /^\s+DOUBLE\s+PRECISION/i){
	    print("#WARNING: skipping double precision in $InFile:$name\n");
	    $double="";
	}
	# uzero
	if ($line =~ /uzero\s*\(/i) {
	    $line =~ s/uzero/uzeroi/i if ($line =~ /uzero\s*\(\s*[i-n,I-N]/i);
	    $line =~ s/uzero/uzero$double/i if ($line =~ /uzero\s*\(\s*[a-h,A-H,o-z,O-Z]/i);
	}
	# vzero
	if ($line =~ /vzero\s*\(/i) {
	    $line =~ s/vzero/vzeroi/i if ($line =~ /vzero\s*\(\s*[i-n,I-N]/i);
	    $line =~ s/vzero/vzero$double/i if ($line =~ /vzero\s*\(\s*[a-h,A-H,o-z,O-Z]/i);
	}
	# vfill
	if ($line =~ /vfill\s*\(/i) {
	    $line =~ s/vfill/vfilli/i if ($line =~ /vfill\s*\(\s*[i-n,I-N]/i);
	    $line =~ s/vfill/vfill$double/i if ($line =~ /vfill\s*\(\s*[a-h,A-H,o-z,O-Z]/i);
	}
	# ucopy
	if ($line =~ /ucopy\s*\(/i) {
	    $line =~ s/ucopy/ucopyi/i if ($line =~ /ucopy\s*\(\s*[i-n,I-N]/i);
	    $line =~ s/ucopy/ucopy$double/i if ($line =~ /ucopy\s*\(\s*[a-h,A-H,o-z,O-Z]/i);
	}
	# ucopy2
	if ($line =~ /ucopy2\s*\(/i) {
	    $line =~ s/ucopy2/ucopy2i/i if ($line =~ /ucopy2\s*\(\s*[i-n,I-N]/i);
	    $line =~ s/ucopy2/ucopy2$double/i if ($line =~ /ucopy2\s*\(\s*[a-h,A-H,o-z,O-Z]/i);
	}
	if ($line ne $orig){
	    print("#INFO replacing\n$orig\n#INFO by\n$line\n");
	}
    }
    print OUT "$line\n";
}
close(IN);
close(OUT);
