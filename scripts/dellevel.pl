#!/usr/bin/perl -w
#+#####################################################################
# File:            $GROUP_DIR/dellevel.pl
# Description:     Produce in output a list of shell commands, that, if
#                  eval'ed, set up the DELPHI environment according to
#                  delana or dstana needs.
#                  The output could be either Bourne shell (-u option)
#                  or C shell compliant (-c option).
# Authors:         Francesco Giacomini (from a prev version of JvE)
# Contact:         delphi-core@delphi-lb.cern.ch
#
# $Log: dellevel.sh,v $
#
# Revision 2.1  1999/08/13           vaneldik
# allow multiple area's (for archived versions...)
#
# Revision 2.0  1999/06/25           vaneldik
# rewrite in Perl, modify shared library path, cleanup
#
# Revision 1.5  1998/11/11           vaneldik
# changed the way ADDLIB is defined
#
# Revision 1.4  1997/12/16  16:18:23 ddurand vaneldik
# changed references to GROUP_DIR for Delshift/Hepix/LSF compatibility
#
# Revision 1.3  1997/02/25  10:41:47  giacomin
# added prerelease version to dstana
#
# Revision 1.2  1996/11/05 13:58:56  giacomin
# added the command pathnames for OSF1
# added the blockdata for OSF1
#
# Revision 1.1  1996/03/06 16:21:00  giacomin
# Initial revision
#
#-#####################################################################

use strict;
use diagnostics;

# get some useful environment variables

my $GROUP_DIR = GetEnv("GROUP_DIR");
my $SHELL     = GetEnv("SHELL");
my $USER      = GetEnv("USER");
my $PATH      = GetEnv("PATH");

my $DEBUG = 0;

# in which area's should we look for package versions?

my %DistributionAreas = ( "dstana" => ["$GROUP_DIR/dstana"],
                          "delana" => ["$GROUP_DIR/delana"],
                          "delsim" => ["$GROUP_DIR/delsim"],
                        );

# get list of available library versions

my @dstanaList = AvailableVersions(@{$DistributionAreas{"dstana"}});
my @delanaList = AvailableVersions(@{$DistributionAreas{"delana"}});
my @delsimList = AvailableVersions(@{$DistributionAreas{"delsim"}});

# set defaults

my $package = "dstana";
my $version = "pro";
undef my $shellSyntax;
if (grep {$_ eq $SHELL} qw(/bin/csh /usr/bin/csh /usr/local/bin/tcsh)){
    $shellSyntax = "C";
}else{
    $shellSyntax = "Bourne";
}

# parse the parameters, overwrite the defaults
while (my $arg = shift @ARGV){
    if ( $arg eq "-p") {
        $package = shift @ARGV;
        $package = "delana" if $package eq "database";
    }elsif ( $arg eq "-v") {
        $version = shift @ARGV;
        $version = "prerelease" if $version eq "pre";
        $version = "pro" if $version eq "new";
    }elsif ( $arg eq "-c") {
        $shellSyntax = "C";
    }elsif ( $arg eq "-u") {
        $shellSyntax = "Bourne";
    }elsif ( $arg eq "-pl") {
        $shellSyntax = "Perl";
    }elsif ( $arg eq "-h") {
        usage();
    }else{
        last if $arg =~ /^\#/;
        usage();
  }
}

# check the parameters

CheckConsistency();

# define DELPHI variables

my %DELPHI_ENV;

$DELPHI_ENV{DELPHI_BIN} = "$GROUP_DIR/bin";

# add /local/delphi on the delb-nodes

undef my $host;
if (defined $ENV{HOST}) {
    $host = $ENV{HOST};
}elsif (defined $ENV{HOSTNAME}) {
    $host = $ENV{HOSTNAME};
}

#if (defined $host) {
#  if ( $host =~ /^delb/ and -d "/local/delphi/bin" ) {
#    #if ($host ne "delb05") { # temp. fix (JvE/010110)
#      $DELPHI_ENV{DELPHI_BIN} = "/local/delphi/bin";
#      unshift(@{$DistributionAreas{$package}},"/local/delphi");
#    #}
#  }
#}

# select the "base" directory

undef my $delphi_base;
for my $area (@{$DistributionAreas{$package}}) {
    if (-d "$area/$version") {
        $delphi_base = "$area/$version";
        last;
    }
}

$DELPHI_ENV{DELPHI} = "$delphi_base";
$DELPHI_ENV{DELPHI_PATH} = "$DELPHI_ENV{DELPHI_BIN}";
$DELPHI_ENV{DELPHI_DAT}  = "$DELPHI_ENV{DELPHI}/dat";
$DELPHI_ENV{DELPHI_PAM}  = "$DELPHI_ENV{DELPHI}/src/car";
$DELPHI_ENV{DELPHI_CRA}  = "$DELPHI_ENV{DELPHI_PAM}";
$DELPHI_ENV{DELPHI_BLKD} = "$DELPHI_ENV{DELPHI}/blkd";
$DELPHI_ENV{DELPHI_LIB}  = "$DELPHI_ENV{DELPHI}/lib";

# define some extra variables, for convenient DELANA running

if ( $package eq "delana" ) {
    $DELPHI_ENV{DELANA_UTY}  =  $DELPHI_ENV{DELPHI_PAM};
    $DELPHI_ENV{DELANA_TAN}  =  $DELPHI_ENV{DELPHI_PAM};
    $DELPHI_ENV{DELANA_PHY}  =  $DELPHI_ENV{DELPHI_PAM};
    $DELPHI_ENV{DELANA_ANA}  =  $DELPHI_ENV{DELPHI_PAM};
}

# Modify the shared library path, if necessary

my %SHLIB = ( "OSF1"   => "LD_LIBRARY_PATH",
              "HPUX10" => "SHLIB_PATH",
              #"AIX"    => "LIBPATH", # don't mess with IBM
              "Linux"  => "LD_LIBRARY_PATH");

undef my $shlib_path;

# set ADDLIB

my $delblkd = "$DELPHI_ENV{DELPHI_BLKD}/delblkd.o";
$DELPHI_ENV{ADDLIB} = (-r $delblkd?$delblkd:'" "');

# output the result in a shell dependent way

if ( $shellSyntax eq "Bourne" ) {
    map {print STDOUT "$_=$DELPHI_ENV{$_}; export $_;\n";} sort keys %DELPHI_ENV;
}elsif ( $shellSyntax eq "C" ) {
    map {print STDOUT "setenv $_ $DELPHI_ENV{$_};\n";} sort keys %DELPHI_ENV;
}elsif ( $shellSyntax eq "Perl" ) {
    map {print STDOUT "\$ENV{\'$_\'} = \"$DELPHI_ENV{$_}\";\n";} sort keys %DELPHI_ENV;
}

# monitor usage of dstana prerelease

#if ( $package eq "dstana" and $version eq "prerelease" ) {
if ( $package eq "dstana" and -e "$ENV{GROUP_DIR}/bin/dellog"){
    if ($version ne $dstanaList[0]) {
        system("$ENV{GROUP_DIR}/bin/dellog dellevel -v $version");
    }
}

exit 0;

sub GetEnv { # return the environment variables
    my $arg = shift;
    return $ENV{$arg} if defined $ENV{$arg};
    print STDERR "Environment variable \$$arg is not defined\n";
    print STDERR "on this machine, exiting...\n";
    exit 1;
}

sub usage { # print the usage

    print STDERR <<EOUSAGE;
 Usage: dellevel [-v version] [-p package] [-h]
  
   package:
     dstana (default)
     delana
   version:
     dstana: pro (default) pre old @dstanaList
     delana: pro (default) pre old @delanaList
     delsim: pro (default) pre old @delsimList
   -h: print this help

EOUSAGE
    exit 1;
}

sub CheckConsistency { # check if the parameters are correct
    if     ( $package eq "dstana" ) {
        if ( $version eq "pro" ) {
            $version = $dstanaList[0];
        }elsif ($version eq "old" ) {
            $version = $dstanaList[1];
        }elsif (not grep /$version$/,@dstanaList) {
            usage();
        }
    }elsif ( $package eq "delana" ) {
        if ( $version eq "pro" ) {
            $version = $delanaList[0];
        }elsif ($version eq "old" ) {
            $version = $delanaList[1];
        }elsif (not grep /$version$/,@delanaList) {
            usage();
        }
        #}elsif ( $package eq "delsim" and $USER eq "vaneldik") {
    }elsif ( $package eq "delsim") {
        if ( $version eq "pro" ) {
            $version = $delsimList[0];
        }elsif ($version eq "old" ) {
            $version = $delsimList[1];
        }elsif (not grep /$version$/,@delsimList) {
            usage();
        }
    }else{
        usage();
  }
}

sub AvailableVersions { # return available versions
    my @dir = @_;
    #undef my (@list,@versions);
    my @list = my @versions = ();
    for my $dir (@dir) {
        if (not opendir(DIR,$dir)) {
            print STDERR "cannot open directory $dir: $!\n";
            exit 1;
        }
        for my $version (readdir(DIR)) {
            next if not -d "$dir/$version";
            #print STDERR "$dir $version\n";
            push(@list,$version) if $version =~ /^\d+$/;
            if ($dir =~ /\/delsim/){
                push(@list,$version) if $version =~ /^v/;
            }
            push(@list,$version) if $version eq "prerelease";
        }
        closedir(DIR);
    }
    # JD's Y2K fix
    my @Y2K = my @NY2K = ();
    map {if (/^(\d{2})/){ if ($1 >= 50) {push(@NY2K,$_)} else {push(@Y2K,$_)}}} grep !/^prerelease$/,@list;
    @versions = ();
    push(@versions,sort {$b <=> $a} grep !/^prerelease$/,@Y2K);
    push(@versions,sort {$b <=> $a} grep !/^prerelease$/,@NY2K);
    #@versions = sort {$b <=> $a} grep !/^prerelease$/ && !/v/,@list;
    # end of Y2K fix
    push(@versions,sort {$b cmp $a} grep /v/,@list);
    push(@versions,"prerelease") if (grep /^prerelease$/,@list);
    #print STDERR "versions in @dir:\n@versions\n" if $DEBUG;
    return @versions;
}


