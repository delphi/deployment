#!/bin/csh -f
#=========================  make PHDST library  =========================
#
#  Usage: phlib.csh [d]
#  Option d -- debug version
#

echo "----> `which $0` is running ... `date`"

set Machine = "`uname`"

if ( $1 == "d" || $1 == "D" ) then
  set suffix = "db"
else
  set suffix = "xx"
endif

switch  ( $Machine )

  case HP-UX:
#      Option +DA1.1 on FORTRAN and C on all HP-UX 10.* . This is to ensure
#      compatibility of libraries versus the different PA-RISC architecture of
#      HP-UX 10.* machines, which actually have PA8000 and PA7200 processors.
#      Without +DA1.1 any library compiled on PA8000 will NOT run on PA7200.
      set Ppatch   = "HPUX, SHIFT"
      set CompileF = "fort77 -c -K +ppu +T "
#      set CompileC = "cc -c -Aa -O -I/usr/local/include "
      set CompileC = "cc -c -Ae -O -I/usr/local/include "
#      if ( `uname -r` =~ *10.* ) then
#        set CompileF = "fort77 -c -K +ppu +T +DA1.1 "
#        set CompileC = "cc -c -Ae -O +DA1.1 -I/usr/local/include "
#      endif
      set Library = "ar r "
      breaksw
  case AIX:
      set Ppatch   = "IBMRT, SHIFT"
      set CompileF = "xlf -c -qextname -qnoescape -qcharlen=8192 "
#      set CompileC = "cc -c -O "
      set CompileC = "gcc -c -O "
      set Library = "ar -vqso "
      breaksw
  case ULTRIX:
      set Ppatch   = "DECS, SHIFT"
      set CompileF = "f77 -c -vms "
      set CompileC = "cc -c "
      set Library  = "ar crl "
      breaksw
  case OSF1:
#      set Ppatch   = "ALPHAOSF, DSTFARM"
      set Ppatch   = "ALPHAOSF, SHIFT"
      set CompileF = "f77 -c -static -assume backslash "
      set CompileC = "cc -c -O2 -I/usr/local/include "
      set Library  = "ar srl "
      breaksw
  case LINUX:
  case Linux:
      set Ppatch   = "LINUX, SHIFT"
      set CompileF = "gfortran $FOPT -c"
      set CompileC = "gcc $COPT -c"
      set Library  = "ar sr "
      breaksw
  case Darwin:
  case DARWIN:
      set Ppatch   = "DARWIN, SHIFT"
      set CompileF = "gfortran $FOPT -c"
      set CompileC = "gcc $COPT -c"
      set Library  = "ar sr "
      breaksw
  default:
      echo '********* Unknown machine code '$Machine
      echo '********* Library NOT created '
      echo '********* Please contact Nikolai.Smirnov@cern.ch'
      exit 1
endsw

if ( $suffix == "db" ) then
  set CompileF = `echo $CompileF -g `
else
  switch  ( $Machine )
    case HP-UX:
#      Change of -O to +O2 for fort77 on all HP-UX. This is because -O is not
#      well recognized on HP-UX 10.20, but +O2 is... Notice that -O = +O2
#      strictly on all other HP systems.
      set CompileF = `echo $CompileF +O2 `
      breaksw
    default:
      set CompileF = `echo $CompileF -O `
      breaksw
  endsw
endif

      echo "----> Generate PHDST${suffix} Library for `uname -a`"
      echo "----> Patchy steering patches: +USE, $Ppatch "
      echo "----> Fortran options used  $CompileF "
      echo "----> C       options used  $CompileC "
      echo "     "

set SUB_DIR = phdst

#***********************************
#    Patchy Run
#***********************************
echo '----> Patching ...'
if (-e phdst.fca) rm phdst.fca
cp $DELPHI_PAM/fatmen.car .
ypatchy - phdst.fca tty tty - - - - - phzip.fca .go  <<EOF
+USE, $Ppatch.
+SELF.   *** FATMEN interface ***
+USE, FATMEN.
+USE, FATCDES.
+USE, FATBUG.
+SELF.   *** GZIP C routines ***
+USE, GZIPH.
+USE, GZIPC, T=E.
+SELF.   *** End of GZIP C routines ***
+SELF.   *** ZLIB compression C routines ***
+USE, USEZLIB.
+USE, ZLZIP, T=E.
+SELF.   *** End of ZLIB compression C routines ***
+EXE.
+USE, PHDST*.
+SELF.
+ASM, 24.
+SELF.
+SELF.+PAM, L=11, T=A,C, R=FATCDES, IF=FATMEN. (CERN_ROOT)/src/car/fatmen.car
+PAM, L=11, T=A,C, R=FATCDES, IF=FATMEN. fatmen.car
+PAM, L=11, T=A,C.                       phdstxx.car
+QUIT.
EOF
rm -f fatmen.car
#***********************************
#    File splitting
#***********************************
echo '----> Spliting ...'

if ( ! ( -d $Machine ) ) then
  mkdir $Machine
endif
cd $Machine

if (-d sub$SUB_DIR) then
  cd sub$SUB_DIR
  rm *
else
  mkdir sub$SUB_DIR
  cd sub$SUB_DIR
endif  
fcasplit  ../../phdst.fca
rm        ../../phdst.fca
fcasplit  ../../phzip.fca
rm        ../../phzip.fca

#***********************************
#    Compilation
#***********************************
echo '----> Compiling FORTRAN ... '
$CompileF *.f
set errorcpF = $status
if ($errorcpF) goto exit

echo '----> Compiling C ... '
$CompileC *.c
set errorcpC = $status
if ($errorcpC) goto exit

#***********************************
#    Library creation
#***********************************
echo '----> Creating archive library '
rm -f    ../libphdst${suffix}.a
$Library ../libphdst${suffix}.a *.o
set errorlb = $status
if ( $Machine == ULTRIX ) ar stl ../libphdst${suffix}.a
#if ( $Machine == OSF1 ) then
#    echo '----> Creating shared library '
#    rm  ../libphdst${suffix}_sl.so
#    f77 -shared -taso -Wl,-soname,libphdst${suffix}_sl.so \
#              -o ../libphdst${suffix}_sl.so *.o \
#              `cernlib`
#    set errorlb = $status
#    if ($errorlb) goto exit
#endif

#
exit:
rm *.o
if ( $errorcpF ) then
    echo "*************************"
    echo "     FORTRAN ERRORS     *"
    echo "*************************"
    exit
endif
if ( $errorcpC ) then
    echo "*******************"
    echo "     C ERRORS     *"
    echo "*******************"
    exit
endif
if ( $errorlb ) then
    echo "*************************"
    echo "     LIBRARY NOT OK     *"
    echo "*************************"
    exit
endif

echo "*********************"
echo "     LIBRARY OK     *"
echo "*********************"
exit
