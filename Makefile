.PHONY: all idea doc ddb dstana simana simanap evsrv des getcodefromgitlab clean swcd export
SHELL=/bin/bash
.SHELLFLAG=-x

#
# defaults for DELPHI environment
#
# for 32bit set the environment variable LEGACY to "-m32"
#
CERNVER = 2025.03.07.0-free
PRO     = 161018
OLD     = 100710
NEW     = 'prerelease'
FFLAGS  = "-g $(LEGACY)"
CFLAGS  = "-g $(LEGACY)"
LDFLAGS = "-g $(LEGACY)"
ADDCOMP = "-g $(LEGACY)"
XROOTD := $(shell [[ -n "$(CERNLIB_XROOTD)" ]] && echo "$(CERNLIB_XROOTD)" || echo "OFF") 
PLINAM  = `uname -s`
DATETAG = `date +%Y%m%d`
DELPHI_SWCD = /tmp/swcd
FALSE = `which false`
TRUE = `which true`

clean:
	echo "DELPHI_INSTALL_DIR will not be cleaned. Do this manually"
	rm -f delphi.sh delphi.csh
	rm -rf /tmp/swcd build

# build the full stack
all: clean setup cernlib dstana ddb des examples docs openphigs delgra idea simana export

swcd:
	./GetCodeFromGitlab.sh $(DELPHI_SWCD)

profiles:
ifeq ("x${DELPHI_INSTALL_DIR}", "x")
	export DELPHI_INSTALL_DIR=/tmp/delphi
endif
# create profiles to setup environment
	cat env/delphi.csh.template | sed -e 's/__PRO__/${PRO}/'| sed -e 's/__ADDCOMP__/${ADDCOMP}/'| sed -e 's/__PLINAM__/${PLINAM}/'| sed -e 's/__LEGACY__/${LEGACY}/g' | sed -e 's/__XROOTD__/${XROOTD}/g' > delphi.csh
	cat env/delphi.sh.template | sed -e 's/__PRO__/${PRO}/'| sed -e 's/__ADDCOMP__/${ADDCOMP}/'| sed -e 's/__PLINAM__/${PLINAM}/'| sed -e 's/__LEGACY__/${LEGACY}/g' | sed -e 's/__XROOTD__/${XROOTD}/g' > delphi.sh
	echo "Done creating the login scripts"

global: profiles
	./make_globalsetup

setup:	profiles global
	. delphi.sh; ./make_groupsetup

logs:
	mkdir logs 2>/dev/null || ${TRUE}

cernlib_im: profiles setup logs
	echo "starting cernlib with imake"
	. delphi.sh; ./make_cernlib $(CERNVER) 0 $(XROOTD) $(LEGACY) > logs/make_cernlib-imake.log 2>&1 || ( cat logs/make_cernlib-imake.log; ${FALSE} )

cernlib_cm: profiles setup logs
	echo "starting cernlib with cmake"
	. delphi.sh; ./make_cernlib $(CERNVER) 1 $(XROOTD) $(LEGACY) > logs/make_cernlib-cmake.log 2>&1 || ( cat logs/make_cernlib-cmake.log; ${FALSE} )

cernlib: profiles setup logs
	echo "starting cernlib, imake build may fail"
	make cernlib_im
	make cernlib_cm

dstana_pre: profiles setup logs
	. delphi.sh; ./make_dstana $(NEW) $(DELPHI_SWCD) > logs/make_dstana_$(NEW).log 2>&1  || ( cat logs/make_dstana_$(NEW).log; ${FALSE} )

dstana_pro: profiles setup logs
	. delphi.sh; ./make_dstana $(PRO) $(DELPHI_SWCD) > logs/make_dstana_$(PRO).log 2>&1  || ( cat logs/make_dstana_$(PRO).log; ${FALSE} )

dstana_old: profiles setup logs
	. delphi.sh; ./make_dstana $(OLD) $(DELPHI_SWCD) > logs/make_dstana_$(OLD).log 2>&1  || ( cat logs/make_dstana_$(OLD).log; ${FALSE} )

dstana: profiles setup logs
	make dstana_pro
	make dstana_pre
	make dstana_old

ddb: profiles setup logs
	. delphi.sh;./make_ddb > logs/make_ddb.log 2>&1 || ( cat logs/make_ddb.log; ${FALSE} )

openphigs: profiles setup logs
	$(eval export OPENPHIGS=$(shell set -x;DELPHI_INSTALL_DIR=$(DELPHI_INSTALL_DIR) source ./delphi.sh ; echo ${OPENPHIGS}))
	. delphi.sh; ./make_openphigs > logs/make_openphigs.log 2>&1 || ( cat logs/make_openphigs.log; ${FALSE} )

delgra:  profiles setup logs
	. delphi.sh; ./make_delgra > logs/make_delgra.log 2>&1 || ( cat logs/make_delgra.log; ${FALSE} )

des:	profiles setup logs
	. delphi.sh; ./make_des > logs/make_des.log 2>&1 || ( cat logs/make_des.log; ${FALSE} )

evsrv:	des

idea: profiles setup logs
	. delphi.sh; ./make_idea > logs/make_idea.log 2>&1  || ( cat logs/make_idea.log; ${FALSE} )

docs: profiles global logs
	. delphi.sh; ./make_docs > logs/make_docs.log 2>&1 || ( cat logs/make_docs.log; ${FALSE} )

examples: profiles global logs
	. delphi.sh; ./make_examples > logs/make_examples.log 2>&1 || ( cat logs/make_examples.log; ${FALSE} )

_simana: profiles setup logs
	. delphi.sh; ./make_simana ${VERS} $(DELPHI_SWCD) 0 > logs/make_simana_${VERS}.log 2>&1 || ( cat logs/make_simana_${VERS}.log ; ${FALSE} )


_delana: profiles setup logs
	. delphi.sh; ./make_simana ${VERS} $(DELPHI_SWCD) 1 > logs/make_simana_${VERS}_rd.log 2>&1 || ( cat logs/make_simana_${VERS}_rd.log ; ${FALSE} )

va0u:
	make _simana VERS=va0u
	make _delana VERS=va0u

va0e:
	make _simana VERS=va0e
	make _delana VERS=va0e

v99e:
	make _simana VERS=v99e
	make _delana VERS=v99e

v98e:
	make _simana VERS=v98e
	make _delana VERS=v98e

v97g:
	make _simana VERS=v97g
	make _delana VERS=v97g

v96g:
	make _simana VERS=v96g
	make _delana VERS=v96g

v95d:
	make _simana VERS=v95d
	make _delana VERS=v95d

v94c:
	make _simana VERS=v94c
	make _delana VERS=v94c

v93d:
	make _simana VERS=v93d
	make _delana VERS=v93d

v92e:
	make _simana VERS=v92e
	make _delana VERS=v92e

v91f:
	make _simana VERS=v91f
	make _delana VERS=v91f

v90e:
#	make _simana VERS=v90e
	make _delana VERS=v90e

simana:
	make va0u > logs/make_simana_va0u.log 2>&1 || cat logs/make_simana_va0u.log
	make va0e > logs/make_simana_va0e.log 2>&1 || cat logs/make_simana_va0e.log
	make v99e > logs/make_simana_v99e.log 2>&1 || cat logs/make_simana_v99e.log
	make v98e > logs/make_simana_v98e.log 2>&1 || cat logs/make_simana_v98e.log
	make v97g > logs/make_simana_v97g.log 2>&1 || cat logs/make_simana_v97g.log
	make v96g > logs/make_simana_v96g.log 2>&1 || cat logs/make_simana_v96g.log
	make v95d > logs/make_simana_v95d.log 2>&1 || cat logs/make_simana_v95d.log
	make v94c > logs/make_simana_v94c.log 2>&1 || cat logs/make_simana_v94c.log
	make v93d > logs/make_simana_v93d.log 2>&1 || cat logs/make_simana_v93d.log
	make v92e > logs/make_simana_v92e.log 2>&1 || cat logs/make_simana_v92e.log
	make v91f > logs/make_simana_v91f.log 2>&1 || cat logs/make_simana_v91f.log
	make v90e > logs/make_simana_v90e.log 2>&1 || cat logs/make_simana_v90e.log

export: profiles setup
	$(eval OS_TAG=$(shell . delphi.sh; echo $$OS_TAG))
	cd $(DELPHI_INSTALL_DIR)/releases ; tar -zcf $(OS_TAG).tgz $(OS_TAG)

test: profiles setup
	. delphi.sh; sh ./make_test.sh

